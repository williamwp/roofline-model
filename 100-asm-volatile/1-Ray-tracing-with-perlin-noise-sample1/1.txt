------------- NVBit (NVidia Binary Instrumentation Tool v1.0) Loaded --------------
NVBit core environment variables (mostly for nvbit-devs):
           NOINSPECT = 0 - if set, skips function inspection and instrumentation
            NVDISASM = nvdisasm - override default nvdisasm found in PATH
            NOBANNER = 0 - if set, does not print this banner
---------------------------------------------------------------------------------
         INSTR_BEGIN = 0 - Beginning of the instruction interval where to apply instrumentation
           INSTR_END = 4294967295 - End of the instruction interval where to apply instrumentation
        KERNEL_BEGIN = 0 - Beginning of the kernel launch interval where to apply instrumentation
          KERNEL_END = 4294967295 - End of the kernel launch interval where to apply instrumentation
    COUNT_WARP_LEVEL = 1 - Count warp level or thread level instructions
    EXCLUDE_PRED_OFF = 0 - Exclude predicated off instruction from count
        TOOL_VERBOSE = 0 - Enable verbosity inside the tool
----------------------------------------------------------------------------------------------------
Loaded image with 1200x600 and 3 channels
Rendering a 1200x600 image (100 samples per pixel) in 4x4 blocks.
Kernel = populate_scene_balls(hitable_object**, hitable_list**, camera**, curandStateXORWOW*, float*)
grid X Y Z = 1, 1, 1
block X Y Z = 1, 1, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 1
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
Kernel = init_rand_state(curandStateXORWOW*, int, int)
grid X Y Z = 301, 151, 1
block X Y Z = 4, 4, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 16
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
Kernel = render(vec3*, int, int, hitable_list**, camera**, curandStateXORWOW*)
grid X Y Z = 301, 151, 1
block X Y Z = 4, 4, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 16
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
took 2255757us.
Kernel = free_scene(hitable_object**, hitable_list**, camera**)
grid X Y Z = 1, 1, 1
block X Y Z = 1, 1, 1
threads(pointer_2->blockDimX * pointer_2->blockDimY * pointer_2->blockDimZ) = 1
shared memory = 0
cuda stream id = 0
------------------------------------------------------------------------------
Deleting sphere object at 0x7f9e519facc0
--Deleting material object at 0x7f9e519fad10
Deleting sphere object at 0x7f9e519fff60
--Deleting material object at 0x7f9e519fffb0
Deleting sphere object at 0x7f9e519fac20
--Deleting material object at 0x7f9e519fac70
Deleting sphere object at 0x7f9e519ffec0
--Deleting material object at 0x7f9e519fff10
Deleting sphere object at 0x7f9e519ff920
--Deleting material object at 0x7f9e519ff970
Deleting sphere object at 0x7f9e519ffe20
--Deleting material object at 0x7f9e519ffe70
Deleting sphere object at 0x7f9e519ffd30
--Deleting material object at 0x7f9e519ffd80
Deleting moving_sphere object at 0x7f9e519fadb0
--Deleting material object at 0x7f9e519fae00
