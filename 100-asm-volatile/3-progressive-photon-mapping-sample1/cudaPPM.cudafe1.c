# 1 "cudaPPM.cu"
# 137 "/usr/include/stdio.h" 3
extern struct _IO_FILE *stderr;
# 19 "cudaPPM.cu"
extern int primes[61];
# 172 "cudaPPM.cu"
unsigned num_hash = 0;
unsigned num_photon = 0;
double hash_scale = 0;
extern struct List *hitpoints;
# 176 "cudaPPM.cu"
struct BoundingBox hit_point_bbox = {{0}};

extern struct HitPoint *hit_points_list;
# 179 "cudaPPM.cu"
extern struct Photon *photon_map;
# 180 "cudaPPM.cu"
extern unsigned *total_count;
# 305 "cudaPPM.cu"
struct Sphere spheres[9];
# 19 "cudaPPM.cu"
int primes[61] = {2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97,101,103,107,109,113,127,131,137,139,149,151,157,163,167,173,179,181,191,193,197,199,211,223,227,229,233,239,241,251,257,263,269,271,277,281,283};
# 175 "cudaPPM.cu"
struct List *hitpoints = ((struct List *)0LL);
# 178 "cudaPPM.cu"
struct HitPoint *hit_points_list = ((struct HitPoint *)0LL);
# 179 "cudaPPM.cu"
struct Photon *photon_map = ((struct Photon *)0LL);
# 180 "cudaPPM.cu"
unsigned *total_count = ((unsigned *)0LL);
