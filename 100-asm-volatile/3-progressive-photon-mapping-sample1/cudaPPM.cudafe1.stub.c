#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-function"
#pragma GCC diagnostic ignored "-Wcast-qual"
#define __NV_CUBIN_HANDLE_STORAGE__ static
#if !defined(__CUDA_INCLUDE_COMPILER_INTERNAL_HEADERS__)
#define __CUDA_INCLUDE_COMPILER_INTERNAL_HEADERS__
#endif
#include "crt/host_runtime.h"
#include "cudaPPM.fatbin.c"
extern void __device_stub__Z12photon_traceP6SphereP6PhotonPjPiif4Vec3j(struct Sphere *, struct Photon *, unsigned *, int *, int, float, struct Vec3&, unsigned);
extern void __device_stub__Z18accumulate_photonsP6PhotonPjP8HitPointi(struct Photon *, unsigned *, struct HitPoint *, int);
static void __nv_cudaEntityRegisterCallback(void **);
static void __sti____cudaRegisterAll(void) __attribute__((__constructor__));
void __device_stub__Z12photon_traceP6SphereP6PhotonPjPiif4Vec3j(struct Sphere *__par0, struct Photon *__par1, unsigned *__par2, int *__par3, int __par4, float __par5, struct Vec3&__par6, unsigned __par7){__cudaLaunchPrologue(8);__cudaSetupArgSimple(__par0, 0UL);__cudaSetupArgSimple(__par1, 8UL);__cudaSetupArgSimple(__par2, 16UL);__cudaSetupArgSimple(__par3, 24UL);__cudaSetupArgSimple(__par4, 32UL);__cudaSetupArgSimple(__par5, 36UL);__cudaSetupArg(__par6, 40UL);__cudaSetupArgSimple(__par7, 64UL);__cudaLaunch(((char *)((void ( *)(struct Sphere *, struct Photon *, unsigned *, int *, int, float, struct Vec3, unsigned))photon_trace)));}
# 427 "cudaPPM.cu"
void photon_trace( struct Sphere *__cuda_0,struct Photon *__cuda_1,unsigned *__cuda_2,int *__cuda_3,int __cuda_4,float __cuda_5,struct Vec3 __cuda_6,unsigned __cuda_7)
# 428 "cudaPPM.cu"
{__device_stub__Z12photon_traceP6SphereP6PhotonPjPiif4Vec3j( __cuda_0,__cuda_1,__cuda_2,__cuda_3,__cuda_4,__cuda_5,__cuda_6,__cuda_7);
# 557 "cudaPPM.cu"
}
# 1 "cudaPPM.cudafe1.stub.c"
void __device_stub__Z18accumulate_photonsP6PhotonPjP8HitPointi( struct Photon *__par0,  unsigned *__par1,  struct HitPoint *__par2,  int __par3) {  __cudaLaunchPrologue(4); __cudaSetupArgSimple(__par0, 0UL); __cudaSetupArgSimple(__par1, 8UL); __cudaSetupArgSimple(__par2, 16UL); __cudaSetupArgSimple(__par3, 24UL); __cudaLaunch(((char *)((void ( *)(struct Photon *, unsigned *, struct HitPoint *, int))accumulate_photons))); }
# 559 "cudaPPM.cu"
void accumulate_photons( struct Photon *__cuda_0,unsigned *__cuda_1,struct HitPoint *__cuda_2,int __cuda_3)
# 560 "cudaPPM.cu"
{__device_stub__Z18accumulate_photonsP6PhotonPjP8HitPointi( __cuda_0,__cuda_1,__cuda_2,__cuda_3);
# 588 "cudaPPM.cu"
}
# 1 "cudaPPM.cudafe1.stub.c"
static void __nv_cudaEntityRegisterCallback( void **__T256) {  __nv_dummy_param_ref(__T256); __nv_save_fatbinhandle_for_managed_rt(__T256); __cudaRegisterEntry(__T256, ((void ( *)(struct Photon *, unsigned *, struct HitPoint *, int))accumulate_photons), _Z18accumulate_photonsP6PhotonPjP8HitPointi, (-1)); __cudaRegisterEntry(__T256, ((void ( *)(struct Sphere *, struct Photon *, unsigned *, int *, int, float, struct Vec3, unsigned))photon_trace), _Z12photon_traceP6SphereP6PhotonPjPiif4Vec3j, (-1)); }
static void __sti____cudaRegisterAll(void) {  __cudaRegisterBinary(__nv_cudaEntityRegisterCallback);  }

#pragma GCC diagnostic pop
