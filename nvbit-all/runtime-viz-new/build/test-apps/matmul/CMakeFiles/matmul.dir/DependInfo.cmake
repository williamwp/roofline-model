
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/home/wangpeng/tools/gitlab-william/roofline-model/nvbit-all/runtime-viz-new/test-apps/matmul/matmul.cu" "test-apps/matmul/CMakeFiles/matmul.dir/matmul.cu.o" "gcc" "test-apps/matmul/CMakeFiles/matmul.dir/matmul.cu.o.d"
  "/home/wangpeng/tools/gitlab-william/roofline-model/nvbit-all/runtime-viz-new/test-apps/matmul/matrix.cu" "test-apps/matmul/CMakeFiles/matmul.dir/matrix.cu.o" "gcc" "test-apps/matmul/CMakeFiles/matmul.dir/matrix.cu.o.d"
  "/home/wangpeng/tools/gitlab-william/roofline-model/nvbit-all/runtime-viz-new/test-apps/matmul/mul_gpu.cu" "test-apps/matmul/CMakeFiles/matmul.dir/mul_gpu.cu.o" "gcc" "test-apps/matmul/CMakeFiles/matmul.dir/mul_gpu.cu.o.d"
  "/home/wangpeng/tools/gitlab-william/roofline-model/nvbit-all/runtime-viz-new/test-apps/matmul/main.cc" "test-apps/matmul/CMakeFiles/matmul.dir/main.cc.o" "gcc" "test-apps/matmul/CMakeFiles/matmul.dir/main.cc.o.d"
  "/home/wangpeng/tools/gitlab-william/roofline-model/nvbit-all/runtime-viz-new/test-apps/matmul/mul_cpu.cc" "test-apps/matmul/CMakeFiles/matmul.dir/mul_cpu.cc.o" "gcc" "test-apps/matmul/CMakeFiles/matmul.dir/mul_cpu.cc.o.d"
  "/home/wangpeng/tools/gitlab-william/roofline-model/nvbit-all/runtime-viz-new/test-apps/matmul/test.cc" "test-apps/matmul/CMakeFiles/matmul.dir/test.cc.o" "gcc" "test-apps/matmul/CMakeFiles/matmul.dir/test.cc.o.d"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/wangpeng/tools/gitlab-william/roofline-model/nvbit-all/runtime-viz-new/build/memtrack/CMakeFiles/memtrack.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
