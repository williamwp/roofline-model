file(REMOVE_RECURSE
  "CMakeFiles/gauss_filter.dir/common.cu.o"
  "CMakeFiles/gauss_filter.dir/common.cu.o.d"
  "CMakeFiles/gauss_filter.dir/conv_cpu.cc.o"
  "CMakeFiles/gauss_filter.dir/conv_cpu.cc.o.d"
  "CMakeFiles/gauss_filter.dir/conv_gpu_all.cu.o"
  "CMakeFiles/gauss_filter.dir/conv_gpu_all.cu.o.d"
  "CMakeFiles/gauss_filter.dir/conv_gpu_cmem.cu.o"
  "CMakeFiles/gauss_filter.dir/conv_gpu_cmem.cu.o.d"
  "CMakeFiles/gauss_filter.dir/conv_gpu_gmem.cu.o"
  "CMakeFiles/gauss_filter.dir/conv_gpu_gmem.cu.o.d"
  "CMakeFiles/gauss_filter.dir/conv_gpu_smem.cu.o"
  "CMakeFiles/gauss_filter.dir/conv_gpu_smem.cu.o.d"
  "CMakeFiles/gauss_filter.dir/filtering.cc.o"
  "CMakeFiles/gauss_filter.dir/filtering.cc.o.d"
  "CMakeFiles/gauss_filter.dir/image.cu.o"
  "CMakeFiles/gauss_filter.dir/image.cu.o.d"
  "CMakeFiles/gauss_filter.dir/main.cc.o"
  "CMakeFiles/gauss_filter.dir/main.cc.o.d"
  "gauss_filter"
  "gauss_filter.pdb"
)

# Per-language clean rules from dependency scanning.
foreach(lang CUDA CXX)
  include(CMakeFiles/gauss_filter.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
