
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/home/wangpeng/tools/gitlab-william/roofline-model/nvbit-all/runtime-viz-new/test-apps/blurring/common.cu" "test-apps/blurring/CMakeFiles/gauss_filter.dir/common.cu.o" "gcc" "test-apps/blurring/CMakeFiles/gauss_filter.dir/common.cu.o.d"
  "/home/wangpeng/tools/gitlab-william/roofline-model/nvbit-all/runtime-viz-new/test-apps/blurring/conv_gpu_all.cu" "test-apps/blurring/CMakeFiles/gauss_filter.dir/conv_gpu_all.cu.o" "gcc" "test-apps/blurring/CMakeFiles/gauss_filter.dir/conv_gpu_all.cu.o.d"
  "/home/wangpeng/tools/gitlab-william/roofline-model/nvbit-all/runtime-viz-new/test-apps/blurring/conv_gpu_cmem.cu" "test-apps/blurring/CMakeFiles/gauss_filter.dir/conv_gpu_cmem.cu.o" "gcc" "test-apps/blurring/CMakeFiles/gauss_filter.dir/conv_gpu_cmem.cu.o.d"
  "/home/wangpeng/tools/gitlab-william/roofline-model/nvbit-all/runtime-viz-new/test-apps/blurring/conv_gpu_gmem.cu" "test-apps/blurring/CMakeFiles/gauss_filter.dir/conv_gpu_gmem.cu.o" "gcc" "test-apps/blurring/CMakeFiles/gauss_filter.dir/conv_gpu_gmem.cu.o.d"
  "/home/wangpeng/tools/gitlab-william/roofline-model/nvbit-all/runtime-viz-new/test-apps/blurring/conv_gpu_smem.cu" "test-apps/blurring/CMakeFiles/gauss_filter.dir/conv_gpu_smem.cu.o" "gcc" "test-apps/blurring/CMakeFiles/gauss_filter.dir/conv_gpu_smem.cu.o.d"
  "/home/wangpeng/tools/gitlab-william/roofline-model/nvbit-all/runtime-viz-new/test-apps/blurring/image.cu" "test-apps/blurring/CMakeFiles/gauss_filter.dir/image.cu.o" "gcc" "test-apps/blurring/CMakeFiles/gauss_filter.dir/image.cu.o.d"
  "/home/wangpeng/tools/gitlab-william/roofline-model/nvbit-all/runtime-viz-new/test-apps/blurring/conv_cpu.cc" "test-apps/blurring/CMakeFiles/gauss_filter.dir/conv_cpu.cc.o" "gcc" "test-apps/blurring/CMakeFiles/gauss_filter.dir/conv_cpu.cc.o.d"
  "/home/wangpeng/tools/gitlab-william/roofline-model/nvbit-all/runtime-viz-new/test-apps/blurring/filtering.cc" "test-apps/blurring/CMakeFiles/gauss_filter.dir/filtering.cc.o" "gcc" "test-apps/blurring/CMakeFiles/gauss_filter.dir/filtering.cc.o.d"
  "/home/wangpeng/tools/gitlab-william/roofline-model/nvbit-all/runtime-viz-new/test-apps/blurring/main.cc" "test-apps/blurring/CMakeFiles/gauss_filter.dir/main.cc.o" "gcc" "test-apps/blurring/CMakeFiles/gauss_filter.dir/main.cc.o.d"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/wangpeng/tools/gitlab-william/roofline-model/nvbit-all/runtime-viz-new/build/memtrack/CMakeFiles/memtrack.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
