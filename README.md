# roofline-model



wangpeng@dacent:~/tools/gitlab-william/roofline-model$ python3 test4.py 
Traceback (most recent call last):
  File "test4.py", line 21, in <module>
    plt.plot([compute_intensity], performance, marker='o', color='blue', label='Program Performance')
  File "/usr/local/lib/python3.7/site-packages/matplotlib/pyplot.py", line 2990, in plot
    **({"data": data} if data is not None else {}), **kwargs)
  File "/usr/local/lib/python3.7/site-packages/matplotlib/axes/_axes.py", line 1605, in plot
    lines = [*self._get_lines(*args, data=data, **kwargs)]
  File "/usr/local/lib/python3.7/site-packages/matplotlib/axes/_base.py", line 315, in __call__
    yield from self._plot_args(this, kwargs)
  File "/usr/local/lib/python3.7/site-packages/matplotlib/axes/_base.py", line 501, in _plot_args
    raise ValueError(f"x and y must have same first dimension, but "
ValueError: x and y must have same first dimension, but have shapes (1,) and (3,)


Traceback (most recent call last):
  File "test3.py", line 21, in <module>
    plt.plot(compute_intensity, performance, marker='o', color='blue', label='Program Performance')
  File "/usr/local/lib/python3.7/site-packages/matplotlib/pyplot.py", line 2990, in plot
    **({"data": data} if data is not None else {}), **kwargs)
  File "/usr/local/lib/python3.7/site-packages/matplotlib/axes/_axes.py", line 1605, in plot
    lines = [*self._get_lines(*args, data=data, **kwargs)]
  File "/usr/local/lib/python3.7/site-packages/matplotlib/axes/_base.py", line 315, in __call__
    yield from self._plot_args(this, kwargs)
  File "/usr/local/lib/python3.7/site-packages/matplotlib/axes/_base.py", line 501, in _plot_args
    raise ValueError(f"x and y must have same first dimension, but "
ValueError: x and y must have same first dimension, but have shapes (1,) and (3,)




# -*- coding:utf-8

```




import matplotlib.pyplot as plt

# 从技术规格中获取硬件信息
peak_performance = 5.3  # 假设单位为 TFLOPs
memory_bandwidth = 732  # 假设单位为 GB/s

# 从程序中获取总浮点运算数和总内存访问次数
total_flops = 1.5e12  # 假设单位为 FLOPs
total_memory_accesses = 3e11  # 假设单位为 次

# 计算计算强度
compute_intensity = total_flops / total_memory_accesses

# 测量程序性能
performance = [2.0, 2.8, 3.5]  # 假设单位为 GFLOPs/s

# 为每个计算强度创建一个对应的性能值
compute_intensity_values = [compute_intensity] * len(performance)

# 绘制Roofline图
plt.figure(figsize=(10, 6))
plt.plot(compute_intensity_values, performance, marker='o', color='blue', label='Program Performance')
plt.axhline(y=peak_performance, color='red', linestyle='--', label='Peak Performance')
plt.axhline(y=memory_bandwidth, color='green', linestyle='--', label='Memory Bandwidth Bound')
plt.xlabel('Compute Intensity (FLOPs/Byte)')
plt.ylabel('Performance (GFLOPs/s)')
plt.title('Roofline Model for Ray Tracing Program on NVIDIA P100')
plt.legend()
plt.grid()
plt.show()







import matplotlib.pyplot as plt

# 从技术规格中获取硬件信息
peak_performance = 5.3  # 假设单位为 TFLOPs
memory_bandwidth = 732  # 假设单位为 GB/s

# 从程序中获取总浮点运算数和总内存访问次数
total_flops = 1.5e12  # 假设单位为 FLOPs
total_memory_accesses = 3e11  # 假设单位为 次

# 计算计算强度
compute_intensity = total_flops / total_memory_accesses

# 测量程序性能
performance = [2.0, 2.8, 3.5]  # 假设单位为 GFLOPs/s

# 绘制Roofline图
plt.figure(figsize=(10, 6))
plt.plot([compute_intensity], performance, marker='o', color='blue', label='Program Performance')
plt.axhline(y=peak_performance, color='red', linestyle='--', label='Peak Performance')
plt.axhline(y=memory_bandwidth, color='green', linestyle='--', label='Memory Bandwidth Bound')
plt.xlabel('Compute Intensity (FLOPs/Byte)')
plt.ylabel('Performance (GFLOPs/s)')
plt.title('Roofline Model for Ray Tracing Program on NVIDIA P100')
plt.legend()
plt.grid()
plt.show()












import matplotlib.pyplot as plt

# 从技术规格中获取硬件信息
peak_performance = 5.3  # 假设单位为 TFLOPs
memory_bandwidth = 732  # 假设单位为 GB/s

# 从程序中获取总浮点运算数和总内存访问次数
total_flops = 1.5e12  # 假设单位为 FLOPs
total_memory_accesses = 3e11  # 假设单位为 次

# 计算计算强度
compute_intensity = total_flops / total_memory_accesses

# 测量程序性能
performance = [2.0, 2.8, 3.5]  # 假设单位为 GFLOPs/s

# 绘制Roofline图
plt.figure(figsize=(10, 6))
plt.plot(compute_intensity, performance, marker='o', color='blue', label='Program Performance')
plt.axhline(y=peak_performance, color='red', linestyle='--', label='Peak Performance')
plt.axhline(y=memory_bandwidth, color='green', linestyle='--', label='Memory Bandwidth Bound')
plt.xlabel('Compute Intensity (FLOPs/Byte)')
plt.ylabel('Performance (GFLOPs/s)')
plt.title('Roofline Model for Ray Tracing Program on NVIDIA P100')
plt.legend()
plt.grid()
plt.show()







import matplotlib.pyplot as plt

# 假设你已经计算得到计算强度和性能
compute_intensity = ...
performance = ...

plt.figure(figsize=(10, 6))
plt.plot(compute_intensity, performance, marker='o', color='blue', label='Program Performance')
plt.axhline(y=peak_performance, color='red', linestyle='--', label='Peak Performance')
plt.axhline(y=memory_bandwidth, color='green', linestyle='--', label='Memory Bandwidth Bound')
plt.xlabel('Compute Intensity (FLOPs/Byte)')
plt.ylabel('Performance')
plt.title('Roofline Model for Ray Tracing Program on NVIDIA P100')
plt.legend()
plt.grid()
plt.show()










import matplotlib.pyplot as plt

# 从技术规格中获取硬件信息（假设以下数值仅为示例）
peak_performance = 5.3  # 假设单位为 TFLOPs
memory_bandwidth = 732  # 假设单位为 GB/s

# 从程序中获取总浮点运算数和总内存访问次数
total_flops = ...
total_memory_accesses = ...

# 计算计算强度
compute_intensity = total_flops / total_memory_accesses

# 测量程序性能（假设以下数值仅为示例）
performance = [2.5, 3.2, 4.1]  # 假设单位为 GFLOPs/s

# 绘制Roofline图
plt.figure(figsize=(10, 6))
plt.plot(compute_intensity, performance, marker='o', color='blue', label='Program Performance')
plt.axhline(y=peak_performance, color='red', linestyle='--', label='Peak Performance')
plt.axhline(y=memory_bandwidth, color='green', linestyle='--', label='Memory Bandwidth Bound')
plt.xlabel('Compute Intensity (FLOPs/Byte)')
plt.ylabel('Performance (GFLOPs/s)')
plt.title('Roofline Model for Ray Tracing Program on NVIDIA P100')
plt.legend()
plt.grid()
plt.show()







```










