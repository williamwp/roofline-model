# -*- coding:utf-8


import matplotlib.pyplot as plt

# 从技术规格中获取硬件信息
peak_performance = 5.3  # 假设单位为 TFLOPs
memory_bandwidth = 732  # 假设单位为 GB/s

# 从程序中获取总浮点运算数和总内存访问次数
total_flops = 1.5e12  # 假设单位为 FLOPs
total_memory_accesses = 3e11  # 假设单位为 次

# 计算计算强度
compute_intensity = total_flops / total_memory_accesses

# 测量程序性能
performance = [2.0, 2.8, 3.5]  # 假设单位为 GFLOPs/s

# 为每个计算强度创建一个对应的性能值
compute_intensity_values = [compute_intensity] * len(performance)

# 绘制Roofline图
plt.figure(figsize=(10, 6))
plt.plot(compute_intensity_values, performance, marker='o', color='blue', label='Program Performance')
plt.axhline(y=peak_performance, color='red', linestyle='--', label='Peak Performance')
plt.axhline(y=memory_bandwidth, color='green', linestyle='--', label='Memory Bandwidth Bound')
plt.xlabel('Compute Intensity (FLOPs/Byte)')
plt.ylabel('Performance (GFLOPs/s)')
plt.title('Roofline Model for Ray Tracing Program on NVIDIA P100')
plt.legend()
plt.grid()
plt.show()

