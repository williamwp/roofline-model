#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-function"
#pragma GCC diagnostic ignored "-Wcast-qual"
#define __NV_CUBIN_HANDLE_STORAGE__ static
#if !defined(__CUDA_INCLUDE_COMPILER_INTERNAL_HEADERS__)
#define __CUDA_INCLUDE_COMPILER_INTERNAL_HEADERS__
#endif
#include "crt/host_runtime.h"
#include "Rendering-background-vec3.fatbin.c"
extern void __device_stub__Z6renderP4vec3ii(struct vec3 *, int, int);
static void __nv_cudaEntityRegisterCallback(void **);
static void __sti____cudaRegisterAll(void) __attribute__((__constructor__));
void __device_stub__Z6renderP4vec3ii(struct vec3 *__par0, int __par1, int __par2){__cudaLaunchPrologue(3);__cudaSetupArgSimple(__par0, 0UL);__cudaSetupArgSimple(__par1, 8UL);__cudaSetupArgSimple(__par2, 12UL);__cudaLaunch(((char *)((void ( *)(struct vec3 *, int, int))render)));}
# 18 "Rendering-background-vec3.cu"
void render( struct vec3 *__cuda_0,int __cuda_1,int __cuda_2)
# 18 "Rendering-background-vec3.cu"
{__device_stub__Z6renderP4vec3ii( __cuda_0,__cuda_1,__cuda_2);
# 24 "Rendering-background-vec3.cu"
}
# 1 "Rendering-background-vec3.cudafe1.stub.c"
static void __nv_cudaEntityRegisterCallback( void **__T6) {  __nv_dummy_param_ref(__T6); __nv_save_fatbinhandle_for_managed_rt(__T6); __cudaRegisterEntry(__T6, ((void ( *)(struct vec3 *, int, int))render), _Z6renderP4vec3ii, (-1)); }
static void __sti____cudaRegisterAll(void) {  __cudaRegisterBinary(__nv_cudaEntityRegisterCallback);  }

#pragma GCC diagnostic pop
