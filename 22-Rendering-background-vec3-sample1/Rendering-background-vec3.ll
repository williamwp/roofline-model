; ModuleID = 'Rendering-background-vec3.cu'
source_filename = "Rendering-background-vec3.cu"
target datalayout = "e-m:e-p270:32:32-p271:32:32-p272:64:64-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

%"class.std::ios_base::Init" = type { i8 }
%"class.std::basic_ostream" = type { i32 (...)**, %"class.std::basic_ios" }
%"class.std::basic_ios" = type { %"class.std::ios_base", %"class.std::basic_ostream"*, i8, i8, %"class.std::basic_streambuf"*, %"class.std::ctype"*, %"class.std::num_put"*, %"class.std::num_get"* }
%"class.std::ios_base" = type { i32 (...)**, i64, i64, i32, i32, i32, %"struct.std::ios_base::_Callback_list"*, %"struct.std::ios_base::_Words", [8 x %"struct.std::ios_base::_Words"], i32, %"struct.std::ios_base::_Words"*, %"class.std::locale" }
%"struct.std::ios_base::_Callback_list" = type { %"struct.std::ios_base::_Callback_list"*, void (i32, %"class.std::ios_base"*, i32)*, i32, i32 }
%"struct.std::ios_base::_Words" = type { i8*, i64 }
%"class.std::locale" = type { %"class.std::locale::_Impl"* }
%"class.std::locale::_Impl" = type { i32, %"class.std::locale::facet"**, i64, %"class.std::locale::facet"**, i8** }
%"class.std::locale::facet" = type <{ i32 (...)**, i32, [4 x i8] }>
%"class.std::basic_streambuf" = type { i32 (...)**, i8*, i8*, i8*, i8*, i8*, i8*, %"class.std::locale" }
%"class.std::ctype" = type <{ %"class.std::locale::facet.base", [4 x i8], %struct.__locale_struct*, i8, [7 x i8], i32*, i32*, i16*, i8, [256 x i8], [256 x i8], i8, [6 x i8] }>
%"class.std::locale::facet.base" = type <{ i32 (...)**, i32 }>
%struct.__locale_struct = type { [13 x %struct.__locale_data*], i16*, i32*, i32*, [13 x i8*] }
%struct.__locale_data = type opaque
%"class.std::num_put" = type { %"class.std::locale::facet.base", [4 x i8] }
%"class.std::num_get" = type { %"class.std::locale::facet.base", [4 x i8] }
%class.vec3 = type { [3 x float] }
%struct.dim3 = type { i32, i32, i32 }
%struct.CUstream_st = type opaque

$_ZN4dim3C2Ejjj = comdat any

$_ZNK4vec31rEv = comdat any

$_ZNK4vec31gEv = comdat any

$_ZNK4vec31bEv = comdat any

@_ZStL8__ioinit = internal global %"class.std::ios_base::Init" zeroinitializer, align 1
@__dso_handle = external hidden global i8
@_ZSt4cerr = external dso_local global %"class.std::basic_ostream", align 8
@.str = private unnamed_addr constant [14 x i8] c"CUDA error = \00", align 1
@.str.1 = private unnamed_addr constant [5 x i8] c" at \00", align 1
@.str.2 = private unnamed_addr constant [2 x i8] c":\00", align 1
@.str.3 = private unnamed_addr constant [3 x i8] c" '\00", align 1
@.str.4 = private unnamed_addr constant [4 x i8] c"' \0A\00", align 1
@.str.5 = private unnamed_addr constant [13 x i8] c"Rendering a \00", align 1
@.str.6 = private unnamed_addr constant [2 x i8] c"x\00", align 1
@.str.7 = private unnamed_addr constant [8 x i8] c" image \00", align 1
@.str.8 = private unnamed_addr constant [4 x i8] c"in \00", align 1
@.str.9 = private unnamed_addr constant [10 x i8] c" blocks.\0A\00", align 1
@.str.10 = private unnamed_addr constant [41 x i8] c"cudaMallocManaged((void **)&fb, fb_size)\00", align 1
@.str.11 = private unnamed_addr constant [29 x i8] c"Rendering-background-vec3.cu\00", align 1
@.str.12 = private unnamed_addr constant [19 x i8] c"cudaGetLastError()\00", align 1
@.str.13 = private unnamed_addr constant [24 x i8] c"cudaDeviceSynchronize()\00", align 1
@.str.14 = private unnamed_addr constant [6 x i8] c"took \00", align 1
@.str.15 = private unnamed_addr constant [11 x i8] c" seconds.\0A\00", align 1
@_ZSt4cout = external dso_local global %"class.std::basic_ostream", align 8
@.str.16 = private unnamed_addr constant [4 x i8] c"P3\0A\00", align 1
@.str.17 = private unnamed_addr constant [2 x i8] c" \00", align 1
@.str.18 = private unnamed_addr constant [6 x i8] c"\0A255\0A\00", align 1
@.str.19 = private unnamed_addr constant [2 x i8] c"\0A\00", align 1
@.str.20 = private unnamed_addr constant [13 x i8] c"cudaFree(fb)\00", align 1
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_Rendering_background_vec3.cu, i8* null }]

; Function Attrs: noinline uwtable
define internal void @__cxx_global_var_init() #0 section ".text.startup" {
entry:
  call void @_ZNSt8ios_base4InitC1Ev(%"class.std::ios_base::Init"* @_ZStL8__ioinit)
  %0 = call i32 @__cxa_atexit(void (i8*)* bitcast (void (%"class.std::ios_base::Init"*)* @_ZNSt8ios_base4InitD1Ev to void (i8*)*), i8* getelementptr inbounds (%"class.std::ios_base::Init", %"class.std::ios_base::Init"* @_ZStL8__ioinit, i32 0, i32 0), i8* @__dso_handle) #2
  ret void
}

declare dso_local void @_ZNSt8ios_base4InitC1Ev(%"class.std::ios_base::Init"*) unnamed_addr #1

declare dso_local void @_ZNSt8ios_base4InitD1Ev(%"class.std::ios_base::Init"*) unnamed_addr #1

; Function Attrs: nounwind
declare dso_local i32 @__cxa_atexit(void (i8*)*, i8*, i8*) #2

; Function Attrs: noinline optnone uwtable
define dso_local void @_Z10check_cuda9cudaErrorPKcS1_i(i32 %result, i8* %func, i8* %file, i32 %line) #3 {
entry:
  %result.addr = alloca i32, align 4
  %func.addr = alloca i8*, align 8
  %file.addr = alloca i8*, align 8
  %line.addr = alloca i32, align 4
  store i32 %result, i32* %result.addr, align 4
  store i8* %func, i8** %func.addr, align 8
  store i8* %file, i8** %file.addr, align 8
  store i32 %line, i32* %line.addr, align 4
  %0 = load i32, i32* %result.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(8) @_ZSt4cerr, i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str, i64 0, i64 0))
  %1 = load i32, i32* %result.addr, align 4
  %call1 = call dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEj(%"class.std::basic_ostream"* %call, i32 %1)
  %call2 = call dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(8) %call1, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.1, i64 0, i64 0))
  %2 = load i8*, i8** %file.addr, align 8
  %call3 = call dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(8) %call2, i8* %2)
  %call4 = call dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(8) %call3, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.2, i64 0, i64 0))
  %3 = load i32, i32* %line.addr, align 4
  %call5 = call dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"* %call4, i32 %3)
  %call6 = call dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(8) %call5, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.3, i64 0, i64 0))
  %4 = load i8*, i8** %func.addr, align 8
  %call7 = call dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(8) %call6, i8* %4)
  %call8 = call dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(8) %call7, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.4, i64 0, i64 0))
  %call9 = call i32 @cudaDeviceReset()
  call void @exit(i32 99) #9
  unreachable

if.end:                                           ; preds = %entry
  ret void
}

declare dso_local dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(8), i8*) #1

declare dso_local dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEj(%"class.std::basic_ostream"*, i32) #1

declare dso_local dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"*, i32) #1

declare dso_local i32 @cudaDeviceReset() #1

; Function Attrs: noreturn nounwind
declare dso_local void @exit(i32) #4

; Function Attrs: noinline norecurse optnone uwtable
define dso_local void @_Z21__device_stub__renderP4vec3ii(%class.vec3* %fb, i32 %max_x, i32 %max_y) #5 {
entry:
  %fb.addr = alloca %class.vec3*, align 8
  %max_x.addr = alloca i32, align 4
  %max_y.addr = alloca i32, align 4
  %grid_dim = alloca %struct.dim3, align 8
  %block_dim = alloca %struct.dim3, align 8
  %shmem_size = alloca i64, align 8
  %stream = alloca i8*, align 8
  %grid_dim.coerce = alloca { i64, i32 }, align 8
  %block_dim.coerce = alloca { i64, i32 }, align 8
  store %class.vec3* %fb, %class.vec3** %fb.addr, align 8
  store i32 %max_x, i32* %max_x.addr, align 4
  store i32 %max_y, i32* %max_y.addr, align 4
  %kernel_args = alloca i8*, i64 3, align 16
  %0 = bitcast %class.vec3** %fb.addr to i8*
  %1 = getelementptr i8*, i8** %kernel_args, i32 0
  store i8* %0, i8** %1, align 8
  %2 = bitcast i32* %max_x.addr to i8*
  %3 = getelementptr i8*, i8** %kernel_args, i32 1
  store i8* %2, i8** %3, align 8
  %4 = bitcast i32* %max_y.addr to i8*
  %5 = getelementptr i8*, i8** %kernel_args, i32 2
  store i8* %4, i8** %5, align 8
  %6 = call i32 @__cudaPopCallConfiguration(%struct.dim3* %grid_dim, %struct.dim3* %block_dim, i64* %shmem_size, i8** %stream)
  %7 = load i64, i64* %shmem_size, align 8
  %8 = load i8*, i8** %stream, align 8
  %9 = bitcast { i64, i32 }* %grid_dim.coerce to i8*
  %10 = bitcast %struct.dim3* %grid_dim to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* align 8 %9, i8* align 8 %10, i64 12, i1 false)
  %11 = getelementptr inbounds { i64, i32 }, { i64, i32 }* %grid_dim.coerce, i32 0, i32 0
  %12 = load i64, i64* %11, align 8
  %13 = getelementptr inbounds { i64, i32 }, { i64, i32 }* %grid_dim.coerce, i32 0, i32 1
  %14 = load i32, i32* %13, align 8
  %15 = bitcast { i64, i32 }* %block_dim.coerce to i8*
  %16 = bitcast %struct.dim3* %block_dim to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* align 8 %15, i8* align 8 %16, i64 12, i1 false)
  %17 = getelementptr inbounds { i64, i32 }, { i64, i32 }* %block_dim.coerce, i32 0, i32 0
  %18 = load i64, i64* %17, align 8
  %19 = getelementptr inbounds { i64, i32 }, { i64, i32 }* %block_dim.coerce, i32 0, i32 1
  %20 = load i32, i32* %19, align 8
  %21 = bitcast i8* %8 to %struct.CUstream_st*
  %call = call i32 @cudaLaunchKernel(i8* bitcast (void (%class.vec3*, i32, i32)* @_Z21__device_stub__renderP4vec3ii to i8*), i64 %12, i32 %14, i64 %18, i32 %20, i8** %kernel_args, i64 %7, %struct.CUstream_st* %21)
  br label %setup.end

setup.end:                                        ; preds = %entry
  ret void
}

declare dso_local i32 @__cudaPopCallConfiguration(%struct.dim3*, %struct.dim3*, i64*, i8**)

declare dso_local i32 @cudaLaunchKernel(i8*, i64, i32, i64, i32, i8**, i64, %struct.CUstream_st*)

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i64(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i64, i1 immarg) #6

; Function Attrs: noinline norecurse optnone uwtable
define dso_local i32 @main() #5 {
entry:
  %retval = alloca i32, align 4
  %nx = alloca i32, align 4
  %ny = alloca i32, align 4
  %tx = alloca i32, align 4
  %ty = alloca i32, align 4
  %num_pixels = alloca i32, align 4
  %fb_size = alloca i64, align 8
  %fb = alloca %class.vec3*, align 8
  %start = alloca i64, align 8
  %stop = alloca i64, align 8
  %blocks = alloca %struct.dim3, align 4
  %threads = alloca %struct.dim3, align 4
  %agg.tmp = alloca %struct.dim3, align 4
  %agg.tmp15 = alloca %struct.dim3, align 4
  %agg.tmp.coerce = alloca { i64, i32 }, align 4
  %agg.tmp15.coerce = alloca { i64, i32 }, align 4
  %timer_seconds = alloca double, align 8
  %j = alloca i32, align 4
  %i = alloca i32, align 4
  %pixel_index = alloca i64, align 8
  %ir = alloca i32, align 4
  %ig = alloca i32, align 4
  %ib = alloca i32, align 4
  store i32 0, i32* %retval, align 4
  store i32 1200, i32* %nx, align 4
  store i32 600, i32* %ny, align 4
  store i32 8, i32* %tx, align 4
  store i32 8, i32* %ty, align 4
  %call = call dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(8) @_ZSt4cerr, i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.5, i64 0, i64 0))
  %0 = load i32, i32* %nx, align 4
  %call1 = call dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"* %call, i32 %0)
  %call2 = call dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(8) %call1, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.6, i64 0, i64 0))
  %1 = load i32, i32* %ny, align 4
  %call3 = call dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"* %call2, i32 %1)
  %call4 = call dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(8) %call3, i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.7, i64 0, i64 0))
  %call5 = call dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(8) @_ZSt4cerr, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.8, i64 0, i64 0))
  %2 = load i32, i32* %tx, align 4
  %call6 = call dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"* %call5, i32 %2)
  %call7 = call dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(8) %call6, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.6, i64 0, i64 0))
  %3 = load i32, i32* %ty, align 4
  %call8 = call dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"* %call7, i32 %3)
  %call9 = call dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(8) %call8, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.9, i64 0, i64 0))
  %4 = load i32, i32* %nx, align 4
  %5 = load i32, i32* %ny, align 4
  %mul = mul nsw i32 %4, %5
  store i32 %mul, i32* %num_pixels, align 4
  %6 = load i32, i32* %num_pixels, align 4
  %conv = sext i32 %6 to i64
  %mul10 = mul i64 %conv, 12
  store i64 %mul10, i64* %fb_size, align 8
  %7 = bitcast %class.vec3** %fb to i8**
  %8 = load i64, i64* %fb_size, align 8
  %call11 = call i32 @cudaMallocManaged(i8** %7, i64 %8, i32 1)
  call void @_Z10check_cuda9cudaErrorPKcS1_i(i32 %call11, i8* getelementptr inbounds ([41 x i8], [41 x i8]* @.str.10, i64 0, i64 0), i8* getelementptr inbounds ([29 x i8], [29 x i8]* @.str.11, i64 0, i64 0), i32 40)
  %call12 = call i64 @clock() #2
  store i64 %call12, i64* %start, align 8
  %9 = load i32, i32* %nx, align 4
  %10 = load i32, i32* %tx, align 4
  %div = sdiv i32 %9, %10
  %add = add nsw i32 %div, 1
  %11 = load i32, i32* %ny, align 4
  %12 = load i32, i32* %ty, align 4
  %div13 = sdiv i32 %11, %12
  %add14 = add nsw i32 %div13, 1
  call void @_ZN4dim3C2Ejjj(%struct.dim3* %blocks, i32 %add, i32 %add14, i32 1)
  %13 = load i32, i32* %tx, align 4
  %14 = load i32, i32* %ty, align 4
  call void @_ZN4dim3C2Ejjj(%struct.dim3* %threads, i32 %13, i32 %14, i32 1)
  %15 = bitcast %struct.dim3* %agg.tmp to i8*
  %16 = bitcast %struct.dim3* %blocks to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* align 4 %15, i8* align 4 %16, i64 12, i1 false)
  %17 = bitcast %struct.dim3* %agg.tmp15 to i8*
  %18 = bitcast %struct.dim3* %threads to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* align 4 %17, i8* align 4 %18, i64 12, i1 false)
  %19 = bitcast { i64, i32 }* %agg.tmp.coerce to i8*
  %20 = bitcast %struct.dim3* %agg.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* align 4 %19, i8* align 4 %20, i64 12, i1 false)
  %21 = getelementptr inbounds { i64, i32 }, { i64, i32 }* %agg.tmp.coerce, i32 0, i32 0
  %22 = load i64, i64* %21, align 4
  %23 = getelementptr inbounds { i64, i32 }, { i64, i32 }* %agg.tmp.coerce, i32 0, i32 1
  %24 = load i32, i32* %23, align 4
  %25 = bitcast { i64, i32 }* %agg.tmp15.coerce to i8*
  %26 = bitcast %struct.dim3* %agg.tmp15 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* align 4 %25, i8* align 4 %26, i64 12, i1 false)
  %27 = getelementptr inbounds { i64, i32 }, { i64, i32 }* %agg.tmp15.coerce, i32 0, i32 0
  %28 = load i64, i64* %27, align 4
  %29 = getelementptr inbounds { i64, i32 }, { i64, i32 }* %agg.tmp15.coerce, i32 0, i32 1
  %30 = load i32, i32* %29, align 4
  %call16 = call i32 @__cudaPushCallConfiguration(i64 %22, i32 %24, i64 %28, i32 %30, i64 0, i8* null)
  %tobool = icmp ne i32 %call16, 0
  br i1 %tobool, label %kcall.end, label %kcall.configok

kcall.configok:                                   ; preds = %entry
  %31 = load %class.vec3*, %class.vec3** %fb, align 8
  %32 = load i32, i32* %nx, align 4
  %33 = load i32, i32* %ny, align 4
  call void @_Z21__device_stub__renderP4vec3ii(%class.vec3* %31, i32 %32, i32 %33)
  br label %kcall.end

kcall.end:                                        ; preds = %kcall.configok, %entry
  %call17 = call i32 @cudaGetLastError()
  call void @_Z10check_cuda9cudaErrorPKcS1_i(i32 %call17, i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.12, i64 0, i64 0), i8* getelementptr inbounds ([29 x i8], [29 x i8]* @.str.11, i64 0, i64 0), i32 48)
  %call18 = call i32 @cudaDeviceSynchronize()
  call void @_Z10check_cuda9cudaErrorPKcS1_i(i32 %call18, i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.13, i64 0, i64 0), i8* getelementptr inbounds ([29 x i8], [29 x i8]* @.str.11, i64 0, i64 0), i32 49)
  %call19 = call i64 @clock() #2
  store i64 %call19, i64* %stop, align 8
  %34 = load i64, i64* %stop, align 8
  %35 = load i64, i64* %start, align 8
  %sub = sub nsw i64 %34, %35
  %conv20 = sitofp i64 %sub to double
  %div21 = fdiv contract double %conv20, 1.000000e+06
  store double %div21, double* %timer_seconds, align 8
  %call22 = call dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(8) @_ZSt4cerr, i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.14, i64 0, i64 0))
  %36 = load double, double* %timer_seconds, align 8
  %call23 = call dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEd(%"class.std::basic_ostream"* %call22, double %36)
  %call24 = call dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(8) %call23, i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.15, i64 0, i64 0))
  %call25 = call dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(8) @_ZSt4cout, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.16, i64 0, i64 0))
  %37 = load i32, i32* %nx, align 4
  %call26 = call dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"* %call25, i32 %37)
  %call27 = call dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(8) %call26, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.17, i64 0, i64 0))
  %38 = load i32, i32* %ny, align 4
  %call28 = call dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"* %call27, i32 %38)
  %call29 = call dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(8) %call28, i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.18, i64 0, i64 0))
  %39 = load i32, i32* %ny, align 4
  %sub30 = sub nsw i32 %39, 1
  store i32 %sub30, i32* %j, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc57, %kcall.end
  %40 = load i32, i32* %j, align 4
  %cmp = icmp sge i32 %40, 0
  br i1 %cmp, label %for.body, label %for.end58

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond31

for.cond31:                                       ; preds = %for.inc, %for.body
  %41 = load i32, i32* %i, align 4
  %42 = load i32, i32* %nx, align 4
  %cmp32 = icmp slt i32 %41, %42
  br i1 %cmp32, label %for.body33, label %for.end

for.body33:                                       ; preds = %for.cond31
  %43 = load i32, i32* %j, align 4
  %44 = load i32, i32* %nx, align 4
  %mul34 = mul nsw i32 %43, %44
  %45 = load i32, i32* %i, align 4
  %add35 = add nsw i32 %mul34, %45
  %conv36 = sext i32 %add35 to i64
  store i64 %conv36, i64* %pixel_index, align 8
  %46 = load %class.vec3*, %class.vec3** %fb, align 8
  %47 = load i64, i64* %pixel_index, align 8
  %arrayidx = getelementptr inbounds %class.vec3, %class.vec3* %46, i64 %47
  %call37 = call float @_ZNK4vec31rEv(%class.vec3* %arrayidx)
  %conv38 = fpext float %call37 to double
  %mul39 = fmul contract double 2.559900e+02, %conv38
  %conv40 = fptosi double %mul39 to i32
  store i32 %conv40, i32* %ir, align 4
  %48 = load %class.vec3*, %class.vec3** %fb, align 8
  %49 = load i64, i64* %pixel_index, align 8
  %arrayidx41 = getelementptr inbounds %class.vec3, %class.vec3* %48, i64 %49
  %call42 = call float @_ZNK4vec31gEv(%class.vec3* %arrayidx41)
  %conv43 = fpext float %call42 to double
  %mul44 = fmul contract double 2.559900e+02, %conv43
  %conv45 = fptosi double %mul44 to i32
  store i32 %conv45, i32* %ig, align 4
  %50 = load %class.vec3*, %class.vec3** %fb, align 8
  %51 = load i64, i64* %pixel_index, align 8
  %arrayidx46 = getelementptr inbounds %class.vec3, %class.vec3* %50, i64 %51
  %call47 = call float @_ZNK4vec31bEv(%class.vec3* %arrayidx46)
  %conv48 = fpext float %call47 to double
  %mul49 = fmul contract double 2.559900e+02, %conv48
  %conv50 = fptosi double %mul49 to i32
  store i32 %conv50, i32* %ib, align 4
  %52 = load i32, i32* %ir, align 4
  %call51 = call dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"* @_ZSt4cout, i32 %52)
  %call52 = call dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(8) %call51, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.17, i64 0, i64 0))
  %53 = load i32, i32* %ig, align 4
  %call53 = call dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"* %call52, i32 %53)
  %call54 = call dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(8) %call53, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.17, i64 0, i64 0))
  %54 = load i32, i32* %ib, align 4
  %call55 = call dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"* %call54, i32 %54)
  %call56 = call dereferenceable(8) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(8) %call55, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.19, i64 0, i64 0))
  br label %for.inc

for.inc:                                          ; preds = %for.body33
  %55 = load i32, i32* %i, align 4
  %inc = add nsw i32 %55, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond31

for.end:                                          ; preds = %for.cond31
  br label %for.inc57

for.inc57:                                        ; preds = %for.end
  %56 = load i32, i32* %j, align 4
  %dec = add nsw i32 %56, -1
  store i32 %dec, i32* %j, align 4
  br label %for.cond

for.end58:                                        ; preds = %for.cond
  %57 = load %class.vec3*, %class.vec3** %fb, align 8
  %58 = bitcast %class.vec3* %57 to i8*
  %call59 = call i32 @cudaFree(i8* %58)
  call void @_Z10check_cuda9cudaErrorPKcS1_i(i32 %call59, i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.20, i64 0, i64 0), i8* getelementptr inbounds ([29 x i8], [29 x i8]* @.str.11, i64 0, i64 0), i32 66)
  %59 = load i32, i32* %retval, align 4
  ret i32 %59
}

declare dso_local i32 @cudaMallocManaged(i8**, i64, i32) #1

; Function Attrs: nounwind
declare dso_local i64 @clock() #7

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local void @_ZN4dim3C2Ejjj(%struct.dim3* %this, i32 %vx, i32 %vy, i32 %vz) unnamed_addr #8 comdat align 2 {
entry:
  %this.addr = alloca %struct.dim3*, align 8
  %vx.addr = alloca i32, align 4
  %vy.addr = alloca i32, align 4
  %vz.addr = alloca i32, align 4
  store %struct.dim3* %this, %struct.dim3** %this.addr, align 8
  store i32 %vx, i32* %vx.addr, align 4
  store i32 %vy, i32* %vy.addr, align 4
  store i32 %vz, i32* %vz.addr, align 4
  %this1 = load %struct.dim3*, %struct.dim3** %this.addr, align 8
  %x = getelementptr inbounds %struct.dim3, %struct.dim3* %this1, i32 0, i32 0
  %0 = load i32, i32* %vx.addr, align 4
  store i32 %0, i32* %x, align 4
  %y = getelementptr inbounds %struct.dim3, %struct.dim3* %this1, i32 0, i32 1
  %1 = load i32, i32* %vy.addr, align 4
  store i32 %1, i32* %y, align 4
  %z = getelementptr inbounds %struct.dim3, %struct.dim3* %this1, i32 0, i32 2
  %2 = load i32, i32* %vz.addr, align 4
  store i32 %2, i32* %z, align 4
  ret void
}

declare dso_local i32 @__cudaPushCallConfiguration(i64, i32, i64, i32, i64, i8*) #1

declare dso_local i32 @cudaGetLastError() #1

declare dso_local i32 @cudaDeviceSynchronize() #1

declare dso_local dereferenceable(8) %"class.std::basic_ostream"* @_ZNSolsEd(%"class.std::basic_ostream"*, double) #1

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local float @_ZNK4vec31rEv(%class.vec3* %this) #8 comdat align 2 {
entry:
  %this.addr = alloca %class.vec3*, align 8
  store %class.vec3* %this, %class.vec3** %this.addr, align 8
  %this1 = load %class.vec3*, %class.vec3** %this.addr, align 8
  %e = getelementptr inbounds %class.vec3, %class.vec3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x float], [3 x float]* %e, i64 0, i64 0
  %0 = load float, float* %arrayidx, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local float @_ZNK4vec31gEv(%class.vec3* %this) #8 comdat align 2 {
entry:
  %this.addr = alloca %class.vec3*, align 8
  store %class.vec3* %this, %class.vec3** %this.addr, align 8
  %this1 = load %class.vec3*, %class.vec3** %this.addr, align 8
  %e = getelementptr inbounds %class.vec3, %class.vec3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x float], [3 x float]* %e, i64 0, i64 1
  %0 = load float, float* %arrayidx, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local float @_ZNK4vec31bEv(%class.vec3* %this) #8 comdat align 2 {
entry:
  %this.addr = alloca %class.vec3*, align 8
  store %class.vec3* %this, %class.vec3** %this.addr, align 8
  %this1 = load %class.vec3*, %class.vec3** %this.addr, align 8
  %e = getelementptr inbounds %class.vec3, %class.vec3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x float], [3 x float]* %e, i64 0, i64 2
  %0 = load float, float* %arrayidx, align 4
  ret float %0
}

declare dso_local i32 @cudaFree(i8*) #1

; Function Attrs: noinline uwtable
define internal void @_GLOBAL__sub_I_Rendering_background_vec3.cu() #0 section ".text.startup" {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="all" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="all" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind }
attributes #3 = { noinline optnone uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="all" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { noreturn nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="all" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { noinline norecurse optnone uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="all" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { argmemonly nounwind willreturn }
attributes #7 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="all" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { noinline nounwind optnone uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="all" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #9 = { noreturn nounwind }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 2, !"SDK Version", [2 x i32] [i32 10, i32 1]}
!1 = !{i32 1, !"wchar_size", i32 4}
!2 = !{!"clang version 11.0.0 (https://github.com/WilliamWangPeng/rvv-llvm.git c438b3de6ce78448710b3be2038d6c2c924a0cfc)"}
