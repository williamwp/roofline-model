
==11392== Profiling result:
==11392== Metric result:
Invocations                               Metric Name                                                    Metric Description         Min         Max         Avg
Device "Tesla P100-PCIE-16GB (0)"
    Kernel: Integrate(float*, float*, float*, int, int, int, int, int, float, float, float, float, float, float*, float*)
         32                             inst_per_warp                                                 Instructions per warp  5.0732e+04  5.1041e+04  5.0948e+04
         32                         branch_efficiency                                                     Branch Efficiency     100.00%     100.00%     100.00%
         32                 warp_execution_efficiency                                             Warp Execution Efficiency      96.07%      96.19%      96.14%
         32         warp_nonpred_execution_efficiency                              Warp Non-Predicated Execution Efficiency      92.20%      92.32%      92.27%
         32                      inst_replay_overhead                                           Instruction Replay Overhead    0.000051    0.000066    0.000058
         32      shared_load_transactions_per_request                           Shared Memory Load Transactions Per Request    0.000000    0.000000    0.000000
         32     shared_store_transactions_per_request                          Shared Memory Store Transactions Per Request    0.000000    0.000000    0.000000
         32       local_load_transactions_per_request                            Local Memory Load Transactions Per Request    0.000000    0.000000    0.000000
         32      local_store_transactions_per_request                           Local Memory Store Transactions Per Request    0.000000    0.000000    0.000000
         32              gld_transactions_per_request                                  Global Load Transactions Per Request   15.699518   15.702455   15.700879
         32              gst_transactions_per_request                                 Global Store Transactions Per Request   22.418085   23.665093   23.090556
         32                 shared_store_transactions                                             Shared Store Transactions           0           0           0
         32                  shared_load_transactions                                              Shared Load Transactions           0           0           0
         32                   local_load_transactions                                               Local Load Transactions           0           0           0
         32                  local_store_transactions                                              Local Store Transactions           0           0           0
         32                          gld_transactions                                              Global Load Transactions  1044602050  1048157754  1047220200
         32                          gst_transactions                                             Global Store Transactions    17950206    25151286    22461628
         32                  sysmem_read_transactions                                       System Memory Read Transactions           0           0           0
         32                 sysmem_write_transactions                                      System Memory Write Transactions           5           5           5
         32                      l2_read_transactions                                                  L2 Read Transactions    67657420    75041120    72934929
         32                     l2_write_transactions                                                 L2 Write Transactions    20348524    31425003    26727002
         32                    dram_read_transactions                                       Device Memory Read Transactions    18565975    33573942    26718343
         32                   dram_write_transactions                                      Device Memory Write Transactions    16521921    24234602    21233349
         32                           global_hit_rate                                     Global Hit Rate in unified l1/tex      45.83%      48.44%      46.69%
         32                            local_hit_rate                                                        Local Hit Rate       0.00%       0.00%       0.00%
         32                  gld_requested_throughput                                      Requested Global Load Throughput  30.179GB/s  43.953GB/s  35.448GB/s
         32                  gst_requested_throughput                                     Requested Global Store Throughput  5.6853GB/s  5.9099GB/s  5.8220GB/s
         32                            gld_throughput                                                Global Load Throughput  241.42GB/s  351.62GB/s  283.58GB/s
         32                            gst_throughput                                               Global Store Throughput  45.483GB/s  47.279GB/s  46.576GB/s
         32                     local_memory_overhead                                                 Local Memory Overhead       0.00%       0.00%       0.00%
         32                        tex_cache_hit_rate                                                Unified Cache Hit Rate      71.96%      73.51%      72.52%
         32                      l2_tex_read_hit_rate                                           L2 Hit Rate (Texture Reads)      60.15%      77.99%      69.14%
         32                     l2_tex_write_hit_rate                                          L2 Hit Rate (Texture Writes)      73.71%      86.66%      81.28%
         32                      dram_read_throughput                                         Device Memory Read Throughput  47.589GB/s  61.675GB/s  55.403GB/s
         32                     dram_write_throughput                                        Device Memory Write Throughput  41.879GB/s  45.104GB/s  44.029GB/s
         32                      tex_cache_throughput                                              Unified Cache Throughput  479.73GB/s  671.09GB/s  542.88GB/s
         32                    l2_tex_read_throughput                                         L2 Throughput (Texture Reads)  124.47GB/s  186.97GB/s  151.29GB/s
         32                   l2_tex_write_throughput                                        L2 Throughput (Texture Writes)  45.483GB/s  47.279GB/s  46.576GB/s
         32                        l2_read_throughput                                                 L2 Throughput (Reads)  124.29GB/s  187.51GB/s  151.24GB/s
         32                       l2_write_throughput                                                L2 Throughput (Writes)  51.786GB/s  58.021GB/s  55.421GB/s
         32                    sysmem_read_throughput                                         System Memory Read Throughput  0.00000B/s  0.00000B/s  0.00000B/s
         32                   sysmem_write_throughput                                        System Memory Write Throughput  9.6309KB/s  13.438KB/s  10.870KB/s
         32                     local_load_throughput                                          Local Memory Load Throughput  0.00000B/s  0.00000B/s  0.00000B/s
         32                    local_store_throughput                                         Local Memory Store Throughput  0.00000B/s  0.00000B/s  0.00000B/s
         32                    shared_load_throughput                                         Shared Memory Load Throughput  0.00000B/s  0.00000B/s  0.00000B/s
         32                   shared_store_throughput                                        Shared Memory Store Throughput  0.00000B/s  0.00000B/s  0.00000B/s
         32                            gld_efficiency                                         Global Memory Load Efficiency      12.50%      12.50%      12.50%
         32                            gst_efficiency                                        Global Memory Store Efficiency      12.50%      12.50%      12.50%
         32                    tex_cache_transactions                                            Unified Cache Transactions   261150512   262039438   261805049
         32                             flop_count_dp                           Floating Point Operations(Double Precision)           0           0           0
         32                         flop_count_dp_add                       Floating Point Operations(Double Precision Add)           0           0           0
         32                         flop_count_dp_fma                       Floating Point Operations(Double Precision FMA)           0           0           0
         32                         flop_count_dp_mul                       Floating Point Operations(Double Precision Mul)           0           0           0
         32                             flop_count_sp                           Floating Point Operations(Single Precision)  7346154177  7457307169  7416883698
         32                         flop_count_sp_add                       Floating Point Operations(Single Precision Add)   950853233   957180017   953961417
         32                         flop_count_sp_fma                       Floating Point Operations(Single Precision FMA)  3010093085  3064134617  3043961140
         32                         flop_count_sp_mul                        Floating Point Operation(Single Precision Mul)   375000000   375000000   375000000
         32                     flop_count_sp_special                   Floating Point Operations(Single Precision Special)   267945426   275151282   272461475
         32                             inst_executed                                                 Instructions Executed   405853948   408331956   407583218
         32                               inst_issued                                                   Instructions Issued   405877616   408357475   407606800
         32                          dram_utilization                                             Device Memory Utilization     Low (2)     Low (2)     Low (2)
         32                        sysmem_utilization                                             System Memory Utilization     Low (1)     Low (1)     Low (1)
         32                          stall_inst_fetch                              Issue Stall Reasons (Instructions Fetch)       1.01%       1.28%       1.10%
         32                     stall_exec_dependency                            Issue Stall Reasons (Execution Dependency)       3.40%       3.76%       3.53%
         32                   stall_memory_dependency                                    Issue Stall Reasons (Data Request)      59.41%      62.85%      61.57%
         32                             stall_texture                                         Issue Stall Reasons (Texture)      10.71%      11.25%      11.02%
         32                                stall_sync                                 Issue Stall Reasons (Synchronization)       0.00%       0.00%       0.00%
         32                               stall_other                                           Issue Stall Reasons (Other)      21.43%      24.40%      22.57%
         32          stall_constant_memory_dependency                              Issue Stall Reasons (Immediate constant)       0.00%       0.01%       0.00%
         32                           stall_pipe_busy                                       Issue Stall Reasons (Pipe Busy)       0.03%       0.04%       0.04%
         32                         shared_efficiency                                              Shared Memory Efficiency       0.00%       0.00%       0.00%
         32                                inst_fp_32                                               FP Instructions(Single)  5165460257  5219317665  5200016266
         32                                inst_fp_64                                               FP Instructions(Double)           0           0           0
         32                              inst_integer                                                  Integer Instructions  2008731688  2084693636  2055261224
         32                          inst_bit_convert                                              Bit-Convert Instructions   375500000   375500000   375500000
         32                              inst_control                                             Control-Flow Instructions   910924312   925302592   919924020
         32                        inst_compute_ld_st                                               Load/Store Instructions  2091128938  2097708976  2095258205
         32                                 inst_misc                                                     Misc Instructions  1386249003  1389825663  1388481578
         32           inst_inter_thread_communication                                             Inter-Thread Instructions           0           0           0
         32                               issue_slots                                                           Issue Slots   343189501   345895105   345151199
         32                                 cf_issued                                      Issued Control-Flow Instructions    46575792    46979872    46839907
         32                               cf_executed                                    Executed Control-Flow Instructions    46575792    46979872    46839907
         32                               ldst_issued                                        Issued Load/Store Instructions   264497286   265406054   265114124
         32                             ldst_executed                                      Executed Load/Store Instructions    67506404    67759389    67685705
         32                       atomic_transactions                                                   Atomic Transactions           0           0           0
         32           atomic_transactions_per_request                                       Atomic Transactions Per Request    0.000000    0.000000    0.000000
         32                      l2_atomic_throughput                                       L2 Throughput (Atomic requests)  0.00000B/s  0.00000B/s  0.00000B/s
         32                    l2_atomic_transactions                                     L2 Transactions (Atomic requests)           0           0           0
         32                  l2_tex_read_transactions                                       L2 Transactions (Texture Reads)    67755763    75108924    72962320
         32                     stall_memory_throttle                                 Issue Stall Reasons (Memory Throttle)       0.00%       0.00%       0.00%
         32                        stall_not_selected                                    Issue Stall Reasons (Not Selected)       0.15%       0.21%       0.17%
         32                 l2_tex_write_transactions                                      L2 Transactions (Texture Writes)    17950206    25151286    22461628
         32                             flop_count_hp                             Floating Point Operations(Half Precision)           0           0           0
         32                         flop_count_hp_add                         Floating Point Operations(Half Precision Add)           0           0           0
         32                         flop_count_hp_mul                          Floating Point Operation(Half Precision Mul)           0           0           0
         32                         flop_count_hp_fma                         Floating Point Operations(Half Precision FMA)           0           0           0
         32                                inst_fp_16                                                 HP Instructions(Half)           0           0           0
         32                                       ipc                                                          Executed IPC    0.354729    0.497973    0.410680
         32                                issued_ipc                                                            Issued IPC    0.354750    0.497998    0.410508
         32                    issue_slot_utilization                                                Issue Slot Utilization      15.01%      21.10%      17.38%
         32                             sm_efficiency                                               Multiprocessor Activity      91.93%      96.76%      93.87%
         32                        achieved_occupancy                                                    Achieved Occupancy    0.860173    0.897257    0.878422
         32                  eligible_warps_per_cycle                                       Eligible Warps Per Active Cycle    0.381027    0.537175    0.439752
         32                        shared_utilization                                             Shared Memory Utilization    Idle (0)    Idle (0)    Idle (0)
         32                            l2_utilization                                                  L2 Cache Utilization     Low (1)     Low (1)     Low (1)
         32                           tex_utilization                                             Unified Cache Utilization     Low (3)     Mid (4)     Low (3)
         32                       ldst_fu_utilization                                  Load/Store Function Unit Utilization     Low (1)     Low (1)     Low (1)
         32                         cf_fu_utilization                                Control-Flow Function Unit Utilization     Low (1)     Low (1)     Low (1)
         32                        tex_fu_utilization                                     Texture Function Unit Utilization     Low (3)     Mid (4)     Low (3)
         32                    special_fu_utilization                                     Special Function Unit Utilization     Low (1)     Low (1)     Low (1)
         32             half_precision_fu_utilization                              Half-Precision Function Unit Utilization    Idle (0)    Idle (0)    Idle (0)
         32           single_precision_fu_utilization                            Single-Precision Function Unit Utilization     Low (2)     Low (2)     Low (2)
         32           double_precision_fu_utilization                            Double-Precision Function Unit Utilization    Idle (0)    Idle (0)    Idle (0)
         32                        flop_hp_efficiency                                            FLOP Efficiency(Peak Half)       0.00%       0.00%       0.00%
         32                        flop_sp_efficiency                                          FLOP Efficiency(Peak Single)       4.86%       6.67%       5.47%
         32                        flop_dp_efficiency                                          FLOP Efficiency(Peak Double)       0.00%       0.00%       0.00%
         32                   sysmem_read_utilization                                        System Memory Read Utilization    Idle (0)    Idle (0)    Idle (0)
         32                  sysmem_write_utilization                                       System Memory Write Utilization     Low (1)     Low (1)     Low (1)
         32               pcie_total_data_transmitted                                           PCIe Total Data Transmitted           0        8704        5184
         32                  pcie_total_data_received                                              PCIe Total Data Received           0        2560         256
         32                inst_executed_global_loads                              Warp level instructions for global loads    66529900    66758698    66698190
         32                 inst_executed_local_loads                               Warp level instructions for local loads           0           0           0
         32                inst_executed_shared_loads                              Warp level instructions for shared loads           0           0           0
         32               inst_executed_surface_loads                             Warp level instructions for surface loads           0           0           0
         32               inst_executed_global_stores                             Warp level instructions for global stores      797296     1067640      971514
         32                inst_executed_local_stores                              Warp level instructions for local stores           0           0           0
         32               inst_executed_shared_stores                             Warp level instructions for shared stores           0           0           0
         32              inst_executed_surface_stores                            Warp level instructions for surface stores           0           0           0
         32              inst_executed_global_atomics                  Warp level instructions for global atom and atom cas           0           0           0
         32           inst_executed_global_reductions                         Warp level instructions for global reductions           0           0           0
         32             inst_executed_surface_atomics                 Warp level instructions for surface atom and atom cas           0           0           0
         32          inst_executed_surface_reductions                        Warp level instructions for surface reductions           0           0           0
         32              inst_executed_shared_atomics                  Warp level shared instructions for atom and atom CAS           0           0           0
         32                     inst_executed_tex_ops                                   Warp level instructions for texture           0           0           0
         32                      l2_global_load_bytes       Bytes read from L2 for misses in Unified Cache for global loads  2165504320  2402885632  2334208169
         32                       l2_local_load_bytes        Bytes read from L2 for misses in Unified Cache for local loads           0           0           0
         32                     l2_surface_load_bytes      Bytes read from L2 for misses in Unified Cache for surface loads           0           0           0
         32                           dram_read_bytes                                Total bytes read from DRAM to L2 cache   594111200  1074366144   854986981
         32                          dram_write_bytes                             Total bytes written from L2 cache to DRAM   528701472   775507264   679467184
         32               l2_local_global_store_bytes   Bytes written to L2 from Unified Cache for local and global stores.   574406592   804841152   718772098
         32                 l2_global_reduction_bytes          Bytes written to L2 from Unified cache for global reductions           0           0           0
         32              l2_global_atomic_store_bytes             Bytes written to L2 from Unified cache for global atomics           0           0           0
         32                    l2_surface_store_bytes            Bytes written to L2 from Unified Cache for surface stores.           0           0           0
         32                l2_surface_reduction_bytes         Bytes written to L2 from Unified Cache for surface reductions           0           0           0
         32             l2_surface_atomic_store_bytes    Bytes transferred between Unified Cache and L2 for surface atomics           0           0           0
         32                      global_load_requests              Total number of global load requests from Multiprocessor   261150512   262039438   261805049
         32                       local_load_requests               Total number of local load requests from Multiprocessor           0           0           0
         32                     surface_load_requests             Total number of surface load requests from Multiprocessor           0           0           0
         32                     global_store_requests             Total number of global store requests from Multiprocessor     2669900     3645690     3293075
         32                      local_store_requests              Total number of local store requests from Multiprocessor           0           0           0
         32                    surface_store_requests            Total number of surface store requests from Multiprocessor           0           0           0
         32                    global_atomic_requests            Total number of global atomic requests from Multiprocessor           0           0           0
         32                 global_reduction_requests         Total number of global reduction requests from Multiprocessor           0           0           0
         32                   surface_atomic_requests           Total number of surface atomic requests from Multiprocessor           0           0           0
         32                surface_reduction_requests        Total number of surface reduction requests from Multiprocessor           0           0           0
         32                         sysmem_read_bytes                                              System Memory Read Bytes           0           0           0
         32                        sysmem_write_bytes                                             System Memory Write Bytes         160         160         160
         32                           l2_tex_hit_rate                                                     L2 Cache Hit Rate      63.82%      79.69%      72.03%
         32                     texture_load_requests             Total number of texture Load requests from Multiprocessor           0           0           0
         32                     unique_warps_launched                                              Number of warps launched        8000        8000        8000
wangpeng@dacent:~/tools/gitlab-william/backup6/GPU-rendering-programs/91/tsdf-fusion-new$

