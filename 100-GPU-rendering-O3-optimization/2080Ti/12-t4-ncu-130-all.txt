took 0.406077 seconds.
==PROF== Disconnected from process 22880
[22880] a.out@127.0.0.1
  getColor(int *, Complex *, Complex *), 2023-Dec-13 09:40:05, Context 1, Stream 7
    Section: Command line profiler metrics
    ---------------------------------------------------------------------- --------------- ------------------------------
    dram__bytes_read.sum                                                              byte                            288
    dram__bytes_read.sum.per_second                                           Mbyte/second                           1.21
    dram__bytes_write.sum                                                            Kbyte                          81.66
    dram__bytes_write.sum.per_second                                          Mbyte/second                         342.92
    dram__sectors_read.sum                                                          sector                              9
    dram__sectors_write.sum                                                         sector                          2,552
    l1tex__f_tex2sm_cycles_active.avg.pct_of_peak_sustained_elapsed                      %                              0
    l1tex__lsu_writeback_active.avg.pct_of_peak_sustained_active                         %                           0.22
    l1tex__lsu_writeback_active.sum                                                  cycle                         40,960
    l1tex__t_bytes_pipe_lsu_mem_global_op_ld.sum.per_second                   Gbyte/second                           4.40
    l1tex__t_bytes_pipe_lsu_mem_global_op_st.sum                                     Mbyte                           1.05
    l1tex__t_bytes_pipe_lsu_mem_global_op_st.sum.per_second                   Gbyte/second                           4.40
    l1tex__t_bytes_pipe_lsu_mem_local_op_ld.sum.per_second                     byte/second                              0
    l1tex__t_requests_pipe_lsu_mem_global_op_atom.sum                              request                              0
    l1tex__t_requests_pipe_lsu_mem_global_op_ld.sum                                request                         32,768
    l1tex__t_requests_pipe_lsu_mem_global_op_red.sum                               request                              0
    l1tex__t_requests_pipe_lsu_mem_global_op_st.sum                                request                          8,192
    l1tex__t_requests_pipe_lsu_mem_local_op_ld.sum                                 request                              0
    l1tex__t_requests_pipe_lsu_mem_local_op_st.sum                                 request                              0
    l1tex__t_requests_pipe_tex_mem_surface_op_atom.sum                             request                              0
    l1tex__t_requests_pipe_tex_mem_surface_op_ld.sum                               request                              0
    l1tex__t_requests_pipe_tex_mem_surface_op_red.sum                              request                              0
    l1tex__t_requests_pipe_tex_mem_surface_op_st.sum                               request                              0
    l1tex__t_requests_pipe_tex_mem_texture.sum                                     request                              0
    l1tex__t_sector_hit_rate.pct                                                         %                          49.80
    l1tex__t_sectors_pipe_lsu_mem_global_op.sum                                                                   (!) n/a
    l1tex__t_sectors_pipe_lsu_mem_global_op_ld.sum                                  sector                         32,768
    l1tex__t_sectors_pipe_lsu_mem_global_op_lookup_hit.sum                                                        (!) n/a
    l1tex__t_sectors_pipe_lsu_mem_local_op_ld.sum                                   sector                              0
    l1tex__t_sectors_pipe_lsu_mem_local_op_st.sum                                   sector                              0
    l1tex__t_sectors_pipe_lsu_mem_local_op_st.sum.per_second                 sector/second                              0
    l1tex__t_set_accesses_pipe_lsu_mem_global_op_atom.sum                                                               0
    l1tex__t_set_accesses_pipe_lsu_mem_global_op_red.sum                                                                0
    l1tex__t_set_accesses_pipe_tex_mem_surface_op_atom.sum                                                              0
    l1tex__t_set_accesses_pipe_tex_mem_surface_op_red.sum                                                               0
    l1tex__tex_writeback_active.avg.pct_of_peak_sustained_active                         %                              0
    l1tex__texin_sm2tex_req_cycles_active.avg.pct_of_peak_sustained_elapse               %                           0.00
    d                                                                                                                    
    lts__t_bytes_equiv_l1sectormiss_pipe_lsu_mem_global_op_ld.sum                    Kbyte                           4.35
    lts__t_bytes_equiv_l1sectormiss_pipe_lsu_mem_global_op_st.sum                    Mbyte                           1.05
    lts__t_bytes_equiv_l1sectormiss_pipe_lsu_mem_surface_op_st.sum                                                (!) n/a
    lts__t_bytes_equiv_l1sectormiss_pipe_tex_mem_surface_op_ld.sum                    byte                              0
    lts__t_sector_hit_rate.pct                                                           %                         101.12
    lts__t_sector_op_read_hit_rate.pct                                                   %                          74.80
    lts__t_sector_op_write_hit_rate.pct                                                  %                          99.98
    lts__t_sectors.avg.pct_of_peak_sustained_elapsed                                     %                           0.27
    lts__t_sectors_aperture_sysmem_op_read.sum                                      sector                              0
    lts__t_sectors_aperture_sysmem_op_read.sum.per_second                    sector/second                              0
    lts__t_sectors_aperture_sysmem_op_write.sum                                     sector                              7
    lts__t_sectors_aperture_sysmem_op_write.sum.per_second                  sector/msecond                          29.39
    lts__t_sectors_op_read.sum                                                      sector                            897
    lts__t_sectors_op_read.sum.per_second                                   sector/usecond                           3.77
    lts__t_sectors_op_write.sum                                                     sector                         32,794
    lts__t_sectors_op_write.sum.per_second                                  sector/usecond                         137.71
    lts__t_sectors_srcunit_l1_op_atom.sum                                           sector                              0
    lts__t_sectors_srcunit_l1_op_atom.sum.per_second                         sector/second                              0
    lts__t_sectors_srcunit_tex_op_read.sum                                          sector                            136
    lts__t_sectors_srcunit_tex_op_read.sum.per_second                       sector/msecond                         571.08
    sm__mio2rf_writeback_active.avg.pct_of_peak_sustained_elapsed                        %                           0.11
    sm__pipe_tensor_cycles_active.avg.pct_of_peak_sustained_active                       %                              0
    sm__warps_active.avg.pct_of_peak_sustained_active                                    %                          45.95
    smsp__average_inst_executed_per_warp.ratio                                   inst/warp                       4,860.49
    smsp__cycles_active.avg.pct_of_peak_sustained_elapsed                                %                          84.73
    smsp__inst_executed.avg.per_cycle_active                                    inst/cycle                           0.55
    smsp__inst_executed.sum                                                           inst                     39,817,155
    smsp__inst_executed_op_global_atom.sum                                                                        (!) n/a
    smsp__inst_executed_op_global_ld.sum                                              inst                         32,768
    smsp__inst_executed_op_global_st.sum                                              inst                          8,192
    smsp__inst_executed_op_local_ld.sum                                               inst                              0
    smsp__inst_executed_op_shared_atom.sum                                            inst                              0
    smsp__inst_executed_op_shared_atom_dot_alu.sum                                    inst                              0
    smsp__inst_executed_op_shared_atom_dot_cas.sum                                    inst                              0
    smsp__inst_executed_op_shared_ld.avg.pct_of_peak_sustained_elapsed                   %                              0
    smsp__inst_executed_op_shared_ld.sum                                              inst                              0
    smsp__inst_executed_op_shared_ld.sum.per_second                            inst/second                              0
    smsp__inst_executed_op_shared_st.avg.pct_of_peak_sustained_elapsed                   %                              0
    smsp__inst_executed_op_shared_st.sum                                              inst                              0
    smsp__inst_executed_op_shared_st.sum.per_second                            inst/second                              0
    smsp__inst_executed_op_surface_atom.sum                                           inst                              0
    smsp__inst_executed_op_surface_ld.sum                                             inst                              0
    smsp__inst_executed_op_surface_red.sum                                            inst                              0
    smsp__inst_executed_op_surface_st.sum                                             inst                              0
    smsp__inst_executed_op_texture.sum                                                inst                              0
    smsp__inst_executed_pipe_adu.sum                                                  inst                          8,192
    smsp__inst_executed_pipe_cbu.sum                                                  inst                         41,068
    smsp__inst_executed_pipe_fp16.sum                                                 inst                              0
    smsp__inst_executed_pipe_fp64.avg.pct_of_peak_sustained_active                       %                              0
    smsp__inst_executed_pipe_lsu.avg.pct_of_peak_sustained_active                        %                           0.14
    smsp__inst_executed_pipe_tex.avg.pct_of_peak_sustained_active                        %                              0
    smsp__inst_issued.avg.per_cycle_active                                      inst/cycle                           0.55
    smsp__inst_issued.sum                                                             inst                     39,908,762
    smsp__issue_active.avg.pct_of_peak_sustained_active                                  %                          55.02
    smsp__sass_thread_inst_executed_op_control_pred_on.sum                            inst                     27,526,881
    smsp__sass_thread_inst_executed_op_dadd_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_dfma_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_dmul_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_fadd_pred_on.sum                               inst                    425,029,951
    smsp__sass_thread_inst_executed_op_ffma_pred_on.sum                               inst                    106,100,413
    smsp__sass_thread_inst_executed_op_fmul_pred_on.sum                               inst                    319,768,681
    smsp__sass_thread_inst_executed_op_fp16_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_fp32_pred_on.sum                               inst                    957,208,891
    smsp__sass_thread_inst_executed_op_fp64_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_hadd_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_hfma_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_hmul_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_integer_pred_on.sum                            inst                     81,083,546
    smsp__sass_thread_inst_executed_op_inter_thread_communication_pred_on.            inst                              0
    sum                                                                                                                  
    smsp__sass_thread_inst_executed_op_memory_pred_on.sum                             inst                      1,310,720
    smsp__sass_thread_inst_executed_op_misc_pred_on.sum                               inst                      2,283,043
    smsp__sass_thread_inst_executed_op_mufu_pred_on.sum                                                           (!) n/a
    smsp__thread_inst_executed_per_inst_executed.pct                                     %                          91.83
    smsp__thread_inst_executed_per_inst_executed.ratio                                                              29.38
    smsp__warp_issue_stalled_drain_miss_per_warp_active.pct                                                       (!) n/a
    smsp__warp_issue_stalled_imc_miss_per_warp_active.pct                                %                           0.09
    smsp__warp_issue_stalled_long_scoreboard_miss_per_warp_active.pct                                             (!) n/a
    smsp__warp_issue_stalled_misc_membar_per_warp_active.pct                                                      (!) n/a
    smsp__warp_issue_stalled_misc_mio_throttle_per_warp_active.pct                                                (!) n/a
    smsp__warp_issue_stalled_misc_miss_per_warp_active.pct                                                        (!) n/a
    smsp__warp_issue_stalled_misc_sleeping_per_warp_active.pct                                                    (!) n/a
    smsp__warp_issue_stalled_misc_tex_throttle_per_warp_active.pct                                                (!) n/a
    smsp__warp_issue_stalled_no_instruction_miss_per_warp_active.pct                                              (!) n/a
    smsp__warp_issue_stalled_short_scoreboard_miss_per_warp_active.pct                                            (!) n/a
    smsp__warps_eligible.sum.per_cycle_active                                         warp                         359.34
    ---------------------------------------------------------------------- --------------- ------------------------------

test@test-C9Z490-PGW:~/tools/backup6/GPU-rendering-programs/12-Mandelbrot-with-OpenGL-sample1$ 
