==35632== Profiling application: ./a.out
==35632== Profiling result:
==35632== Metric result:
Invocations                               Metric Name                                                    Metric Description         Min         Max         Avg
Device "Tesla P100-PCIE-16GB (0)"
    Kernel: render_kernel(int, int, int, Polygon*, float3*, float3*, float3, float3)
         24                             inst_per_warp                                                 Instructions per warp  1.2032e+05  1.2039e+05  1.2032e+05
         24                         branch_efficiency                                                     Branch Efficiency      87.97%      87.97%      87.97%
         24                 warp_execution_efficiency                                             Warp Execution Efficiency      72.94%      72.99%      72.98%
         24         warp_nonpred_execution_efficiency                              Warp Non-Predicated Execution Efficiency      70.07%      70.12%      70.11%
         24                      inst_replay_overhead                                           Instruction Replay Overhead    0.000027    0.000029    0.000028
         24      shared_load_transactions_per_request                           Shared Memory Load Transactions Per Request    0.000000    0.000000    0.000000
         24     shared_store_transactions_per_request                          Shared Memory Store Transactions Per Request    0.000000    0.000000    0.000000
         24       local_load_transactions_per_request                            Local Memory Load Transactions Per Request    0.000000    0.000000    0.000000
         24      local_store_transactions_per_request                           Local Memory Store Transactions Per Request    0.000000    0.000000    0.000000
         24              gld_transactions_per_request                                  Global Load Transactions Per Request   15.088964   15.088964   15.088964
         24              gst_transactions_per_request                                 Global Store Transactions Per Request   16.000000   16.000000   16.000000
         24                 shared_store_transactions                                             Shared Store Transactions           0           0           0
         24                  shared_load_transactions                                              Shared Load Transactions           0           0           0
         24                   local_load_transactions                                               Local Load Transactions           0           0           0
         24                  local_store_transactions                                              Local Store Transactions           0           0           0
         24                          gld_transactions                                              Global Load Transactions  4821872642  4821872642  4821872642
         24                          gst_transactions                                             Global Store Transactions     2396160     2396160     2396160
         24                  sysmem_read_transactions                                       System Memory Read Transactions           0           0           0
         24                 sysmem_write_transactions                                      System Memory Write Transactions           5           5           5
         24                      l2_read_transactions                                                  L2 Read Transactions     1005918     1011696     1008304
         24                     l2_write_transactions                                                 L2 Write Transactions     2396178     2396723     2396261
         24                    dram_read_transactions                                       Device Memory Read Transactions      299713      301154      300378
         24                   dram_write_transactions                                      Device Memory Write Transactions      603641      606610      605176
         24                           global_hit_rate                                     Global Hit Rate in unified l1/tex      99.72%      99.72%      99.72%
         24                            local_hit_rate                                                        Local Hit Rate       0.00%       0.00%       0.00%
         24                  gld_requested_throughput                                      Requested Global Load Throughput  32.928GB/s  54.674GB/s  46.530GB/s
         24                  gst_requested_throughput                                     Requested Global Store Throughput  466.12MB/s  773.84MB/s  658.60MB/s
         24                            gld_throughput                                                Global Load Throughput  275.36GB/s  457.20GB/s  389.10GB/s
         24                            gst_throughput                                               Global Store Throughput  1.8208GB/s  3.0228GB/s  2.5727GB/s
         24                     local_memory_overhead                                                 Local Memory Overhead       0.00%       0.00%       0.00%
         24                        tex_cache_hit_rate                                                Unified Cache Hit Rate      99.75%      99.75%      99.75%
         24                      l2_tex_read_hit_rate                                           L2 Hit Rate (Texture Reads)      70.10%      70.32%      70.23%
         24                     l2_tex_write_hit_rate                                          L2 Hit Rate (Texture Writes)      91.67%      91.67%      91.67%
         24                      dram_read_throughput                                         Device Memory Read Throughput  233.80MB/s  388.38MB/s  330.25MB/s
         24                     dram_write_throughput                                        Device Memory Write Throughput  470.49MB/s  783.61MB/s  665.35MB/s
         24                      tex_cache_throughput                                              Unified Cache Throughput  971.31GB/s  1612.5GB/s  1372.4GB/s
         24                    l2_tex_read_throughput                                         L2 Throughput (Texture Reads)  783.91MB/s  1.2694GB/s  1.0807GB/s
         24                   l2_tex_write_throughput                                        L2 Throughput (Texture Writes)  1.8208GB/s  3.0228GB/s  2.5727GB/s
         24                        l2_read_throughput                                                 L2 Throughput (Reads)  784.41MB/s  1.2721GB/s  1.0826GB/s
         24                       l2_write_throughput                                                L2 Throughput (Writes)  1.8208GB/s  3.0228GB/s  2.5728GB/s
         24                    sysmem_read_throughput                                         System Memory Read Throughput  0.00000B/s  0.00000B/s  0.00000B/s
         24                   sysmem_write_throughput                                        System Memory Write Throughput  3.9834KB/s  6.6133KB/s  5.6279KB/s
         24                     local_load_throughput                                          Local Memory Load Throughput  0.00000B/s  0.00000B/s  0.00000B/s
         24                    local_store_throughput                                         Local Memory Store Throughput  0.00000B/s  0.00000B/s  0.00000B/s
         24                    shared_load_throughput                                         Shared Memory Load Throughput  0.00000B/s  0.00000B/s  0.00000B/s
         24                   shared_store_throughput                                        Shared Memory Store Throughput  0.00000B/s  0.00000B/s  0.00000B/s
         24                            gld_efficiency                                         Global Memory Load Efficiency      11.96%      11.96%      11.96%
         24                            gst_efficiency                                        Global Memory Store Efficiency      25.00%      25.00%      25.00%
         24                    tex_cache_transactions                                            Unified Cache Transactions  1278251520  1278251520  1278251520
         24                             flop_count_dp                           Floating Point Operations(Double Precision)    71884800    71884800    71884800
         24                         flop_count_dp_add                       Floating Point Operations(Double Precision Add)    14376960    14376960    14376960
         24                         flop_count_dp_fma                       Floating Point Operations(Double Precision FMA)    28753920    28753920    28753920
         24                         flop_count_dp_mul                       Floating Point Operations(Double Precision Mul)           0           0           0
         24                             flop_count_sp                           Floating Point Operations(Single Precision)  4.4954e+10  4.4959e+10  4.4956e+10
         24                         flop_count_sp_add                       Floating Point Operations(Single Precision Add)  9621161195  9622387895  9621774083
         24                         flop_count_sp_fma                       Floating Point Operations(Single Precision FMA)  1.4868e+10  1.4869e+10  1.4869e+10
         24                         flop_count_sp_mul                        Floating Point Operation(Single Precision Mul)  5597067969  5597892055  5597482689
         24                     flop_count_sp_special                   Floating Point Operations(Single Precision Special)   904920434   905115580   905091390
         24                             inst_executed                                                 Instructions Executed  3003089182  3005038801  3003244744
         24                               inst_issued                                                   Instructions Issued  3003174105  3005121173  3003328718
         24                          dram_utilization                                             Device Memory Utilization     Low (1)     Low (1)     Low (1)
         24                        sysmem_utilization                                             System Memory Utilization     Low (1)     Low (1)     Low (1)
         24                          stall_inst_fetch                              Issue Stall Reasons (Instructions Fetch)      13.85%      13.86%      13.86%
         24                     stall_exec_dependency                            Issue Stall Reasons (Execution Dependency)      13.84%      13.84%      13.84%
         24                   stall_memory_dependency                                    Issue Stall Reasons (Data Request)       4.29%       4.32%       4.30%
         24                             stall_texture                                         Issue Stall Reasons (Texture)       0.87%       0.88%       0.87%
         24                                stall_sync                                 Issue Stall Reasons (Synchronization)       0.00%       0.00%       0.00%
         24                               stall_other                                           Issue Stall Reasons (Other)      60.43%      60.47%      60.45%
         24          stall_constant_memory_dependency                              Issue Stall Reasons (Immediate constant)       0.01%       0.01%       0.01%
         24                           stall_pipe_busy                                       Issue Stall Reasons (Pipe Busy)       0.58%       0.58%       0.58%
         24                         shared_efficiency                                              Shared Memory Efficiency       0.00%       0.00%       0.00%
         24                                inst_fp_32                                               FP Instructions(Single)  3.5406e+10  3.5410e+10  3.5408e+10
         24                                inst_fp_64                                               FP Instructions(Double)    57507840    57507840    57507840
         24                              inst_integer                                                  Integer Instructions  1.0636e+10  1.0638e+10  1.0636e+10
         24                          inst_bit_convert                                              Bit-Convert Instructions   134184960   134184960   134184960
         24                              inst_control                                             Control-Flow Instructions  4719688956  4720666722  4719768634
         24                        inst_compute_ld_st                                               Load/Store Instructions  1.0231e+10  1.0231e+10  1.0231e+10
         24                                 inst_misc                                                     Misc Instructions  6192191724  6193086284  6192387612
         24           inst_inter_thread_communication                                             Inter-Thread Instructions           0           0           0
         24                               issue_slots                                                           Issue Slots  2750567607  2752248756  2750703794
         24                                 cf_issued                                      Issued Control-Flow Instructions   339361570   339955469   339399893
         24                               cf_executed                                    Executed Control-Flow Instructions   339361570   339955469   339399893
         24                               ldst_issued                                        Issued Load/Store Instructions  1278950400  1278950400  1278950400
         24                             ldst_executed                                      Executed Load/Store Instructions   319812480   319812480   319812480
         24                       atomic_transactions                                                   Atomic Transactions           0           0           0
         24           atomic_transactions_per_request                                       Atomic Transactions Per Request    0.000000    0.000000    0.000000
         24                      l2_atomic_throughput                                       L2 Throughput (Atomic requests)  0.00000B/s  0.00000B/s  0.00000B/s
         24                    l2_atomic_transactions                                     L2 Transactions (Atomic requests)           0           0           0
         24                  l2_tex_read_transactions                                       L2 Transactions (Texture Reads)     1005354     1007760     1006541
         24                     stall_memory_throttle                                 Issue Stall Reasons (Memory Throttle)       0.00%       0.00%       0.00%
         24                        stall_not_selected                                    Issue Stall Reasons (Not Selected)       6.07%       6.08%       6.08%
         24                 l2_tex_write_transactions                                      L2 Transactions (Texture Writes)     2396160     2396160     2396160
         24                             flop_count_hp                             Floating Point Operations(Half Precision)           0           0           0
         24                         flop_count_hp_add                         Floating Point Operations(Half Precision Add)           0           0           0
         24                         flop_count_hp_mul                          Floating Point Operation(Half Precision Mul)           0           0           0
         24                         flop_count_hp_fma                         Floating Point Operations(Half Precision FMA)           0           0           0
         24                                inst_fp_16                                                 HP Instructions(Half)           0           0           0
         24                                       ipc                                                          Executed IPC    1.718407    1.719665    1.719285
         24                                issued_ipc                                                            Issued IPC    1.718840    1.719887    1.719385
         24                    issue_slot_utilization                                                Issue Slot Utilization      78.71%      78.76%      78.74%
         24                             sm_efficiency                                               Multiprocessor Activity      98.71%      99.38%      99.33%
         24                        achieved_occupancy                                                    Achieved Occupancy    0.442516    0.442621    0.442579
         24                  eligible_warps_per_cycle                                       Eligible Warps Per Active Cycle    3.192289    3.195753    3.194340
         24                        shared_utilization                                             Shared Memory Utilization    Idle (0)    Idle (0)    Idle (0)
         24                            l2_utilization                                                  L2 Cache Utilization     Low (1)     Low (1)     Low (1)
         24                           tex_utilization                                             Unified Cache Utilization    High (8)    High (8)    High (8)
         24                       ldst_fu_utilization                                  Load/Store Function Unit Utilization     Low (1)     Low (1)     Low (1)
         24                         cf_fu_utilization                                Control-Flow Function Unit Utilization     Low (2)     Low (2)     Low (2)
         24                        tex_fu_utilization                                     Texture Function Unit Utilization    High (8)    High (8)    High (8)
         24                    special_fu_utilization                                     Special Function Unit Utilization     Low (1)     Low (1)     Low (1)
         24             half_precision_fu_utilization                              Half-Precision Function Unit Utilization    Idle (0)    Idle (0)    Idle (0)
         24           single_precision_fu_utilization                            Single-Precision Function Unit Utilization    Max (10)    Max (10)    Max (10)
         24           double_precision_fu_utilization                            Double-Precision Function Unit Utilization     Low (1)     Low (1)     Low (1)
         24                        flop_hp_efficiency                                            FLOP Efficiency(Peak Half)       0.00%       0.00%       0.00%
         24                        flop_sp_efficiency                                          FLOP Efficiency(Peak Single)      19.85%      19.98%      19.97%
         24                        flop_dp_efficiency                                          FLOP Efficiency(Peak Double)       0.06%       0.06%       0.06%
         24                   sysmem_read_utilization                                        System Memory Read Utilization    Idle (0)    Idle (0)    Idle (0)
         24                  sysmem_write_utilization                                       System Memory Write Utilization     Low (1)     Low (1)     Low (1)
         24               pcie_total_data_transmitted                                           PCIe Total Data Transmitted        5120      109056       33258
         24                  pcie_total_data_received                                              PCIe Total Data Received           0       87040       23509
         24                inst_executed_global_loads                              Warp level instructions for global loads   319562880   319562880   319562880
         24                 inst_executed_local_loads                               Warp level instructions for local loads           0           0           0
         24                inst_executed_shared_loads                              Warp level instructions for shared loads           0           0           0
         24               inst_executed_surface_loads                             Warp level instructions for surface loads           0           0           0
         24               inst_executed_global_stores                             Warp level instructions for global stores      149760      149760      149760
         24                inst_executed_local_stores                              Warp level instructions for local stores           0           0           0
         24               inst_executed_shared_stores                             Warp level instructions for shared stores           0           0           0
         24              inst_executed_surface_stores                            Warp level instructions for surface stores           0           0           0
         24              inst_executed_global_atomics                  Warp level instructions for global atom and atom cas           0           0           0
         24           inst_executed_global_reductions                         Warp level instructions for global reductions           0           0           0
         24             inst_executed_surface_atomics                 Warp level instructions for surface atom and atom cas           0           0           0
         24          inst_executed_surface_reductions                        Warp level instructions for surface reductions           0           0           0
         24              inst_executed_shared_atomics                  Warp level shared instructions for atom and atom CAS           0           0           0
         24                     inst_executed_tex_ops                                   Warp level instructions for texture           0           0           0
         24                      l2_global_load_bytes       Bytes read from L2 for misses in Unified Cache for global loads    32177248    32256704    32211026
         24                       l2_local_load_bytes        Bytes read from L2 for misses in Unified Cache for local loads           0           0           0
         24                     l2_surface_load_bytes      Bytes read from L2 for misses in Unified Cache for surface loads           0           0           0
         24                           dram_read_bytes                                Total bytes read from DRAM to L2 cache     9590816     9636928     9612100
         24                          dram_write_bytes                             Total bytes written from L2 cache to DRAM    19316512    19411520    19365633
         24               l2_local_global_store_bytes   Bytes written to L2 from Unified Cache for local and global stores.    76677120    76677120    76677120
         24                 l2_global_reduction_bytes          Bytes written to L2 from Unified cache for global reductions           0           0           0
         24              l2_global_atomic_store_bytes             Bytes written to L2 from Unified cache for global atomics           0           0           0
         24                    l2_surface_store_bytes            Bytes written to L2 from Unified Cache for surface stores.           0           0           0
         24                l2_surface_reduction_bytes         Bytes written to L2 from Unified Cache for surface reductions           0           0           0
         24             l2_surface_atomic_store_bytes    Bytes transferred between Unified Cache and L2 for surface atomics           0           0           0
         24                      global_load_requests              Total number of global load requests from Multiprocessor  1278251520  1278251520  1278251520
         24                       local_load_requests               Total number of local load requests from Multiprocessor           0           0           0
         24                     surface_load_requests             Total number of surface load requests from Multiprocessor           0           0           0
         24                     global_store_requests             Total number of global store requests from Multiprocessor      599040      599040      599040
         24                      local_store_requests              Total number of local store requests from Multiprocessor           0           0           0
         24                    surface_store_requests            Total number of surface store requests from Multiprocessor           0           0           0
         24                    global_atomic_requests            Total number of global atomic requests from Multiprocessor           0           0           0
         24                 global_reduction_requests         Total number of global reduction requests from Multiprocessor           0           0           0
         24                   surface_atomic_requests           Total number of surface atomic requests from Multiprocessor           0           0           0
         24                surface_reduction_requests        Total number of surface reduction requests from Multiprocessor           0           0           0
         24                         sysmem_read_bytes                                              System Memory Read Bytes           0           0           0
         24                        sysmem_write_bytes                                             System Memory Write Bytes         160         160         160
         24                           l2_tex_hit_rate                                                     L2 Cache Hit Rate      85.30%      85.36%      85.32%
         24                     texture_load_requests             Total number of texture Load requests from Multiprocessor           0           0           0
         24                     unique_warps_launched                                              Number of warps launched       24960       24960       24960
wangpeng@dacent:~/tools/gitlab-william/backup6/GPU-rendering-programs/13-LiteTracer-sample1$ 
