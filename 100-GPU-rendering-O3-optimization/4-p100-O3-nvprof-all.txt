Replaying kernel "kernel(float*, unsigned long, unsigned long, unsigned long, unsigned long, float, float, int, float, float, unsigned int, float, float, float, float)" (done)
Time to generate:  567171.1 ms; Framerate: 0.00 images/sec
output file name : 22.png
time elapsed : 1690.04
***************************


==29151== Profiling application: ./film_grain_rendering_main 2-li-bai.png 22.png
==29151== Profiling result:
==29151== Metric result:
Invocations                               Metric Name                                                    Metric Description         Min         Max         Avg
Device "Tesla P100-PCIE-16GB (0)"
    Kernel: kernel(float*, unsigned long, unsigned long, unsigned long, unsigned long, float, float, int, float, float, unsigned int, float, float, float, float)
          3                             inst_per_warp                                                 Instructions per warp  1.3839e+06  1.4433e+06  1.4201e+06
          3                         branch_efficiency                                                     Branch Efficiency      88.87%      89.31%      89.08%
          3                 warp_execution_efficiency                                             Warp Execution Efficiency      44.37%      46.69%      45.28%
          3         warp_nonpred_execution_efficiency                              Warp Non-Predicated Execution Efficiency      42.55%      44.87%      43.47%
          3                      inst_replay_overhead                                           Instruction Replay Overhead    0.000015    0.000016    0.000015
          3      shared_load_transactions_per_request                           Shared Memory Load Transactions Per Request    0.000000    0.000000    0.000000
          3     shared_store_transactions_per_request                          Shared Memory Store Transactions Per Request    0.000000    0.000000    0.000000
          3       local_load_transactions_per_request                            Local Memory Load Transactions Per Request    0.000000    0.000000    0.000000
          3      local_store_transactions_per_request                           Local Memory Store Transactions Per Request    3.902901    3.902901    3.902901
          3              gld_transactions_per_request                                  Global Load Transactions Per Request    0.000000    0.000000    0.000000
          3              gst_transactions_per_request                                 Global Store Transactions Per Request    5.655284    5.655284    5.655284
          3                 shared_store_transactions                                             Shared Store Transactions           0           0           0
          3                  shared_load_transactions                                              Shared Load Transactions           0           0           0
          3                   local_load_transactions                                               Local Load Transactions           0           0           0
          3                  local_store_transactions                                              Local Store Transactions   554112000   554112000   554112000
          3                          gld_transactions                                              Global Load Transactions           2           2           2
          3                          gst_transactions                                             Global Store Transactions      501816      501816      501816
          3                  sysmem_read_transactions                                       System Memory Read Transactions           0           0           0
          3                 sysmem_write_transactions                                      System Memory Write Transactions           5           5           5
          3                      l2_read_transactions                                                  L2 Read Transactions    10897067    14792612    13019640
          3                     l2_write_transactions                                                 L2 Write Transactions   554629160   554646176   554636851
          3                    dram_read_transactions                                       Device Memory Read Transactions     1452476     1462776     1456732
          3                   dram_write_transactions                                      Device Memory Write Transactions      395146      397582      396090
          3                           global_hit_rate                                     Global Hit Rate in unified l1/tex       0.00%       0.00%       0.00%
          3                            local_hit_rate                                                        Local Hit Rate       0.00%       0.00%       0.00%
          3                  gld_requested_throughput                                      Requested Global Load Throughput  0.00000B/s  0.00000B/s  0.00000B/s
          3                  gst_requested_throughput                                     Requested Global Store Throughput  6.1447MB/s  7.6500MB/s  6.7388MB/s
          3                            gld_throughput                                                Global Load Throughput  0.00000B/s  0.00000B/s  0.00000B/s
          3                            gst_throughput                                               Global Store Throughput  8.9223MB/s  11.108MB/s  9.7848MB/s
          3                     local_memory_overhead                                                 Local Memory Overhead      97.37%      98.01%      97.64%
          3                        tex_cache_hit_rate                                                Unified Cache Hit Rate      97.56%      97.73%      97.64%
          3                      l2_tex_read_hit_rate                                           L2 Hit Rate (Texture Reads)      84.72%      96.63%      90.55%
          3                     l2_tex_write_hit_rate                                          L2 Hit Rate (Texture Writes)      99.97%      99.97%      99.97%
          3                      dram_read_throughput                                         Device Memory Read Throughput  25.825MB/s  32.379MB/s  28.405MB/s
          3                     dram_write_throughput                                        Device Memory Write Throughput  7.0257MB/s  8.8006MB/s  7.7233MB/s
          3                      tex_cache_throughput                                              Unified Cache Throughput  193.01GB/s  231.52GB/s  210.65GB/s
          3                    l2_tex_read_throughput                                         L2 Throughput (Texture Reads)  234.08MB/s  258.66MB/s  248.90MB/s
          3                   l2_tex_write_throughput                                        L2 Throughput (Texture Writes)  9.6299GB/s  11.989GB/s  10.561GB/s
          3                        l2_read_throughput                                                 L2 Throughput (Reads)  241.21MB/s  263.01MB/s  253.87MB/s
          3                       l2_write_throughput                                                L2 Throughput (Writes)  9.6304GB/s  11.989GB/s  10.561GB/s
          3                    sysmem_read_throughput                                         System Memory Read Throughput  0.00000B/s  0.00000B/s  0.00000B/s
          3                   sysmem_write_throughput                                        System Memory Write Throughput  93.0000B/s  116.000B/s  101.000B/s
          3                     local_load_throughput                                          Local Memory Load Throughput  0.00000B/s  0.00000B/s  0.00000B/s
          3                    local_store_throughput                                         Local Memory Store Throughput  9.6212GB/s  11.978GB/s  10.551GB/s
          3                    shared_load_throughput                                         Shared Memory Load Throughput  0.00000B/s  0.00000B/s  0.00000B/s
          3                   shared_store_throughput                                        Shared Memory Store Throughput  0.00000B/s  0.00000B/s  0.00000B/s
          3                            gld_efficiency                                         Global Memory Load Efficiency       0.00%       0.00%       0.00%
          3                            gst_efficiency                                        Global Memory Store Efficiency      68.87%      68.87%      68.87%
          3                    tex_cache_transactions                                            Unified Cache Transactions  1.0711e+10  1.1360e+10  1.1062e+10
          3                             flop_count_dp                           Floating Point Operations(Double Precision)  3.4841e+11  3.5616e+11  3.5253e+11
          3                         flop_count_dp_add                       Floating Point Operations(Double Precision Add)  2.0743e+10  2.2127e+10  2.1472e+10
          3                         flop_count_dp_fma                       Floating Point Operations(Double Precision FMA)  1.3279e+11  1.3494e+11  1.3394e+11
          3                         flop_count_dp_mul                       Floating Point Operations(Double Precision Mul)  6.2080e+10  6.4156e+10  6.3174e+10
          3                             flop_count_sp                           Floating Point Operations(Single Precision)  3.7199e+11  3.8693e+11  3.7954e+11
          3                         flop_count_sp_add                       Floating Point Operations(Single Precision Add)  2.9161e+10  3.1113e+10  3.0158e+10
          3                         flop_count_sp_fma                       Floating Point Operations(Single Precision FMA)  1.3912e+11  1.4536e+11  1.4226e+11
          3                         flop_count_sp_mul                        Floating Point Operation(Single Precision Mul)  6.4596e+10  6.5105e+10  6.4859e+10
          3                     flop_count_sp_special                   Floating Point Operations(Single Precision Special)  2.4932e+10  2.5824e+10  2.5381e+10
          3                             inst_executed                                                 Instructions Executed  1.2414e+11  1.2946e+11  1.2738e+11
          3                               inst_issued                                                   Instructions Issued  1.2414e+11  1.2946e+11  1.2738e+11
          3                          dram_utilization                                             Device Memory Utilization     Low (1)     Low (1)     Low (1)
          3                        sysmem_utilization                                             System Memory Utilization     Low (1)     Low (1)     Low (1)
          3                          stall_inst_fetch                              Issue Stall Reasons (Instructions Fetch)      13.22%      13.38%      13.30%
          3                     stall_exec_dependency                            Issue Stall Reasons (Execution Dependency)      25.51%      25.58%      25.55%
          3                   stall_memory_dependency                                    Issue Stall Reasons (Data Request)       2.74%       2.98%       2.86%
          3                             stall_texture                                         Issue Stall Reasons (Texture)       0.00%       0.00%       0.00%
          3                                stall_sync                                 Issue Stall Reasons (Synchronization)       0.00%       0.00%       0.00%
          3                               stall_other                                           Issue Stall Reasons (Other)      50.12%      50.39%      50.24%
          3          stall_constant_memory_dependency                              Issue Stall Reasons (Immediate constant)       0.00%       0.00%       0.00%
          3                           stall_pipe_busy                                       Issue Stall Reasons (Pipe Busy)       3.96%       4.01%       3.99%
          3                         shared_efficiency                                              Shared Memory Efficiency       0.00%       0.00%       0.00%
          3                                inst_fp_32                                               FP Instructions(Single)  3.2695e+11  3.3815e+11  3.3258e+11
          3                                inst_fp_64                                               FP Instructions(Double)  2.4570e+11  2.4985e+11  2.4789e+11
          3                              inst_integer                                                  Integer Instructions  5.8881e+11  6.0953e+11  5.9835e+11
          3                          inst_bit_convert                                              Bit-Convert Instructions  2.5187e+11  2.5529e+11  2.5339e+11
          3                              inst_control                                             Control-Flow Instructions  1.3463e+11  1.3709e+11  1.3564e+11
          3                        inst_compute_ld_st                                               Load/Store Instructions  6.3955e+10  6.7179e+10  6.5455e+10
          3                                 inst_misc                                                     Misc Instructions  1.3536e+11  1.4046e+11  1.3780e+11
          3           inst_inter_thread_communication                                             Inter-Thread Instructions           0           0           0
          3                               issue_slots                                                           Issue Slots  1.1199e+11  1.1682e+11  1.1493e+11
          3                                 cf_issued                                      Issued Control-Flow Instructions  1.7243e+10  1.8231e+10  1.7822e+10
          3                               cf_executed                                    Executed Control-Flow Instructions  1.7243e+10  1.8231e+10  1.7822e+10
          3                               ldst_issued                                        Issued Load/Store Instructions  1.2119e+10  1.2768e+10  1.2470e+10
          3                             ldst_executed                                      Executed Load/Store Instructions  2592646575  2634424293  2620371598
          3                       atomic_transactions                                                   Atomic Transactions           0           0           0
          3           atomic_transactions_per_request                                       Atomic Transactions Per Request    0.000000    0.000000    0.000000
          3                      l2_atomic_throughput                                       L2 Throughput (Atomic requests)  0.00000B/s  0.00000B/s  0.00000B/s
          3                    l2_atomic_transactions                                     L2 Transactions (Atomic requests)           0           0           0
          3                  l2_tex_read_transactions                                       L2 Transactions (Texture Reads)    10575117    14155610    12764640
          3                     stall_memory_throttle                                 Issue Stall Reasons (Memory Throttle)       0.00%       0.00%       0.00%
          3                        stall_not_selected                                    Issue Stall Reasons (Not Selected)       4.01%       4.10%       4.05%
          3                 l2_tex_write_transactions                                      L2 Transactions (Texture Writes)   554613816   554613816   554613816
          3                             flop_count_hp                             Floating Point Operations(Half Precision)           0           0           0
          3                         flop_count_hp_add                         Floating Point Operations(Half Precision Add)           0           0           0
          3                         flop_count_hp_mul                          Floating Point Operation(Half Precision Mul)           0           0           0
          3                         flop_count_hp_fma                         Floating Point Operations(Half Precision FMA)           0           0           0
          3                                inst_fp_16                                                 HP Instructions(Half)           0           0           0
          3                                       ipc                                                          Executed IPC    1.429193    1.435274    1.431862
          3                                issued_ipc                                                            Issued IPC    1.429214    1.435296    1.431884
          3                    issue_slot_utilization                                                Issue Slot Utilization      64.48%      64.74%      64.60%
          3                             sm_efficiency                                               Multiprocessor Activity      99.28%      99.34%      99.30%
          3                        achieved_occupancy                                                    Achieved Occupancy    0.339113    0.339895    0.339554
          3                  eligible_warps_per_cycle                                       Eligible Warps Per Active Cycle    2.108783    2.130024    2.119043
          3                        shared_utilization                                             Shared Memory Utilization    Idle (0)    Idle (0)    Idle (0)
          3                            l2_utilization                                                  L2 Cache Utilization     Low (1)     Low (1)     Low (1)
          3                           tex_utilization                                             Unified Cache Utilization     Low (2)     Low (2)     Low (2)
          3                       ldst_fu_utilization                                  Load/Store Function Unit Utilization     Low (1)     Low (1)     Low (1)
          3                         cf_fu_utilization                                Control-Flow Function Unit Utilization     Low (2)     Low (2)     Low (2)
          3                        tex_fu_utilization                                     Texture Function Unit Utilization     Low (2)     Low (2)     Low (2)
          3                    special_fu_utilization                                     Special Function Unit Utilization     Low (3)     Low (3)     Low (3)
          3             half_precision_fu_utilization                              Half-Precision Function Unit Utilization    Idle (0)    Idle (0)    Idle (0)
          3           single_precision_fu_utilization                            Single-Precision Function Unit Utilization     Low (3)     Mid (4)     Low (3)
          3           double_precision_fu_utilization                            Double-Precision Function Unit Utilization     Low (2)     Low (2)     Low (2)
          3                        flop_hp_efficiency                                            FLOP Efficiency(Peak Half)       0.00%       0.00%       0.00%
          3                        flop_sp_efficiency                                          FLOP Efficiency(Peak Single)       3.28%       3.34%       3.31%
          3                        flop_dp_efficiency                                          FLOP Efficiency(Peak Double)       6.10%       6.25%       6.15%
          3                   sysmem_read_utilization                                        System Memory Read Utilization    Idle (0)    Idle (0)    Idle (0)
          3                  sysmem_write_utilization                                       System Memory Write Utilization     Low (1)     Low (1)     Low (1)
          3               pcie_total_data_transmitted                                           PCIe Total Data Transmitted     1427456     2143232     1683285
          3                  pcie_total_data_received                                              PCIe Total Data Received      975360     1716736     1233237
          3                inst_executed_global_loads                              Warp level instructions for global loads           0           0           0
          3                 inst_executed_local_loads                               Warp level instructions for local loads           0           0           0
          3                inst_executed_shared_loads                              Warp level instructions for shared loads           0           0           0
          3               inst_executed_surface_loads                             Warp level instructions for surface loads           0           0           0
          3               inst_executed_global_stores                             Warp level instructions for global stores       88734       88734       88734
          3                inst_executed_local_stores                              Warp level instructions for local stores   141974400   141974400   141974400
          3               inst_executed_shared_stores                             Warp level instructions for shared stores           0           0           0
          3              inst_executed_surface_stores                            Warp level instructions for surface stores           0           0           0
          3              inst_executed_global_atomics                  Warp level instructions for global atom and atom cas           0           0           0
          3           inst_executed_global_reductions                         Warp level instructions for global reductions           0           0           0
          3             inst_executed_surface_atomics                 Warp level instructions for surface atom and atom cas           0           0           0
          3          inst_executed_surface_reductions                        Warp level instructions for surface reductions           0           0           0
          3              inst_executed_shared_atomics                  Warp level shared instructions for atom and atom CAS           0           0           0
          3                     inst_executed_tex_ops                                   Warp level instructions for texture  1598023305  1639801023  1625748328
          3                      l2_global_load_bytes       Bytes read from L2 for misses in Unified Cache for global loads           0           0           0
          3                       l2_local_load_bytes        Bytes read from L2 for misses in Unified Cache for local loads           0           0           0
          3                     l2_surface_load_bytes      Bytes read from L2 for misses in Unified Cache for surface loads           0           0           0
          3                           dram_read_bytes                                Total bytes read from DRAM to L2 cache    46479232    46808832    46615424
          3                          dram_write_bytes                             Total bytes written from L2 cache to DRAM    12644672    12722624    12674880
          3               l2_local_global_store_bytes   Bytes written to L2 from Unified Cache for local and global stores.  1.7748e+10  1.7748e+10  1.7748e+10
          3                 l2_global_reduction_bytes          Bytes written to L2 from Unified cache for global reductions           0           0           0
          3              l2_global_atomic_store_bytes             Bytes written to L2 from Unified cache for global atomics           0           0           0
          3                    l2_surface_store_bytes            Bytes written to L2 from Unified Cache for surface stores.           0           0           0
          3                l2_surface_reduction_bytes         Bytes written to L2 from Unified Cache for surface reductions           0           0           0
          3             l2_surface_atomic_store_bytes    Bytes transferred between Unified Cache and L2 for surface atomics           0           0           0
          3                      global_load_requests              Total number of global load requests from Multiprocessor           0           0           0
          3                       local_load_requests               Total number of local load requests from Multiprocessor           0      102896       34298
          3                     surface_load_requests             Total number of surface load requests from Multiprocessor           0           0           0
          3                     global_store_requests             Total number of global store requests from Multiprocessor      347040      347040      347040
          3                      local_store_requests              Total number of local store requests from Multiprocessor   555264000   555264000   555264000
          3                    surface_store_requests            Total number of surface store requests from Multiprocessor           0           0           0
          3                    global_atomic_requests            Total number of global atomic requests from Multiprocessor           0           0           0
          3                 global_reduction_requests         Total number of global reduction requests from Multiprocessor           0           0           0
          3                   surface_atomic_requests           Total number of surface atomic requests from Multiprocessor           0           0           0
          3                surface_reduction_requests        Total number of surface reduction requests from Multiprocessor           0           0           0
          3                         sysmem_read_bytes                                              System Memory Read Bytes           0           0           0
          3                        sysmem_write_bytes                                             System Memory Write Bytes         160         160         160
          3                           l2_tex_hit_rate                                                     L2 Cache Hit Rate      99.65%      99.80%      99.71%
          3                     texture_load_requests             Total number of texture Load requests from Multiprocessor  1.0711e+10  1.1360e+10  1.1062e+10
          3                     unique_warps_launched                                              Number of warps launched       89700       89700       89700
wangpeng@dacent:~/tools/gitlab-william/backup6/GPU-rendering-programs/4-Film-grain-rendering-gpu-sample1/bin$ 
