import matplotlib.pyplot as plt

compute_intensity = 100
performance = 0.5

plt.figure(figsize=(10, 6))
plt.plot(compute_intensity, performance, marker='o', color='blue', label='Program Performance')
plt.axhline(y=peak_performance, color='red', linestyle='--', label='Peak Performance')
plt.axhline(y=memory_bandwidth, color='green', linestyle='--', label='Memory Bandwidth Bound')
plt.xlabel('Compute Intensity (FLOPs/Byte)')
plt.ylabel('Performance')
plt.title('Roofline Model for Ray Tracing Program on NVIDIA P100')
plt.legend()
plt.grid()
plt.show()
